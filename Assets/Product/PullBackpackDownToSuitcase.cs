﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PullBackpackDownToSuitcase : MonoBehaviour
{
    public GameObject backpack;

    public bool isTriggered = false;


    public bool isTriggering = false;
    private Vector3 backpackInitV3 = new Vector3(0, 0, 0f);
    private Vector3 backpackTargetV3 = new Vector3(0, -1.5f, 0);
    private Vector3 backpackTranslateV3 = new Vector3(0, -0.1f, 0);
    private int backpackNR = 15;
    private int backpackRCount = 0;


    void Update()
    {
        if (Input.GetKey("t"))
        {
            this.isTriggered = true;
        }

        if (this.isTriggered)
        {
            if (!this.isTriggering)
            {
                this.isTriggering = true;
                this.backpackRCount = 0;
                backpack.transform.localPosition = this.backpackInitV3;
            }
            this.isTriggered = !this.action();
            if (!this.isTriggered)
            {
                this.isTriggering = false;
                backpack.transform.localPosition = this.backpackTargetV3;
            }
        }
    }

    bool action()
    {
        bool isFinished = true;
        if (this.backpackRCount < this.backpackNR)
        {
            isFinished = false;
            backpack.transform.localPosition = this.backpackInitV3 + this.backpackTranslateV3 * this.backpackRCount;
            this.backpackRCount += 1;
        }
        return isFinished;
    }
}
