﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetSuitcase : MonoBehaviour
{
    public GameObject front;
    public GameObject bottom;
    public GameObject mainPocket;

    public bool isTriggered = false;

    void Update()
    {

        if (Input.GetKey("r"))
        {
            this.isTriggered = true;
        }

        if (this.isTriggered)
        {
            this.isTriggered = !this.action();
        }
    }

    bool action()
    {
        this.transform.localEulerAngles = Vector3.zero;
        bottom.transform.localEulerAngles = Vector3.zero;
        front.transform.localEulerAngles = Vector3.zero;
        mainPocket.transform.localEulerAngles = new Vector3(0, 90, 0);
        return true;
    }
}
