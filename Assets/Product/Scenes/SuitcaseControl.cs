﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuitcaseControl : MonoBehaviour
{
    public GameObject movingSuitcase;
    public GameObject layingSuitcase;
    public Camera mainCamera;

    public GameObject[] fList;
    public GameObject[] f7List;

    private Vector3 initCameraAngle;
    private float initFoV;
    private int fIndex = 1;
    private Zoom zoomScript;
    private Zoom zoomOutScript;
    private TouchCameraWithRotation cameraScript;
    private CloseSuitcase closeSuitcaseScript;
    private OpenSuitcase openSuitcaseScript;
    private RotateFront rotateFrontScript;
    private LayDownSuitcase layDownSuitcase;
    private PullHandle pullHandle;
    private FlyHanger flyHanger;

    // Start is called before the first frame update
    void Start()
    {
        initCameraAngle = mainCamera.transform.localEulerAngles;
        initFoV = mainCamera.fieldOfView;
        zoomScript = this.GetComponents<Zoom>()[0];
        zoomOutScript = this.GetComponents<Zoom>()[1];
        cameraScript = this.GetComponent<TouchCameraWithRotation>();
        closeSuitcaseScript = movingSuitcase.GetComponent<CloseSuitcase>();
        openSuitcaseScript = movingSuitcase.GetComponent<OpenSuitcase>();
        rotateFrontScript = movingSuitcase.GetComponent<RotateFront>();
        layDownSuitcase = movingSuitcase.GetComponent<LayDownSuitcase>();
        pullHandle = movingSuitcase.GetComponent<PullHandle>();
        flyHanger = layingSuitcase.GetComponent<FlyHanger>();

        movingSuitcase.SetActive(false);
        layingSuitcase.SetActive(false);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey("q"))
        {
            this.showHideSuitcase("movingSuitcase");
        }
        //else if (Input.GetKey("w"))
        //{
        //    this.showHideSuitcase("layingSuitcase");
        //}
        else if (Input.GetKey("f"))
        {
            this.showHideFunctionalities("show");
        }
        else if (Input.GetKey("g"))
        {
            this.showHideFunctionalities("hide");
        }
        else if (Input.GetKey("1"))
        {
            this.zoomFunctionalities("1");
        }
        else if (Input.GetKey("2"))
        {
            this.zoomFunctionalities("2");
        }
        else if (Input.GetKey("3"))
        {
            this.zoomFunctionalities("3");
        }
        else if (Input.GetKey("4"))
        {
            this.zoomFunctionalities("4");
        }
        else if (Input.GetKey("5")) this.zoomFunctionalities("5");
        else if (Input.GetKey("6")) this.zoomFunctionalities("6");
        else if (Input.GetKey("7")) this.zoomFunctionalities("7");
        else if (Input.GetKey("8")) this.zoomFunctionalities("8");
        else if (Input.GetKey("9")) this.zoomFunctionalities("9");
        else if (Input.GetKey("0")) this.zoomFunctionalities("10");
        else if (Input.GetKey("-")) this.zoomFunctionalities("11");
        else if (Input.GetKey("=")) this.zoomFunctionalities("12");
    }

    public void resetCamera(string name)
    {
        mainCamera.fieldOfView = initFoV;
        mainCamera.transform.localEulerAngles = initCameraAngle;
        cameraScript.isEnabled = true;
        cameraScript.reset(null);

        if (name.Equals("movingSuitcase"))
        {
            movingSuitcase.transform.position = new Vector3(-2, -4, 0);
            movingSuitcase.transform.localEulerAngles = new Vector3(0, 0, 0);
            movingSuitcase.transform.Find("Bottom").localEulerAngles = new Vector3(0, 270, 0);
            movingSuitcase.transform.Find("Front").localEulerAngles = new Vector3(0, 90, 0);
            movingSuitcase.transform.Find("Main_pocket").localEulerAngles = new Vector3(0, 90, 0);
        }
        else if (name.Equals("movingSuitcaseBack"))
        {
            movingSuitcase.transform.position = new Vector3(2, -4, -2);
            movingSuitcase.transform.localEulerAngles = new Vector3(0, 230, 0);
            movingSuitcase.transform.Find("Bottom").localEulerAngles = new Vector3(0, 270, 0);
            movingSuitcase.transform.Find("Front").localEulerAngles = new Vector3(0, 90, 0);
            movingSuitcase.transform.Find("Main_pocket").localEulerAngles = new Vector3(0, 90, 0);
        }
        else if (name.Equals("movingSuitcaseOpenPre"))
        {
            movingSuitcase.transform.position = new Vector3(0, -2, 4);
            movingSuitcase.transform.localEulerAngles = new Vector3(0, 0, 0);
            movingSuitcase.transform.Find("Bottom").localEulerAngles = new Vector3(0, 270, 0);
            movingSuitcase.transform.Find("Front").localEulerAngles = new Vector3(0, 90, 0);
            movingSuitcase.transform.Find("Main_pocket").localEulerAngles = new Vector3(0, 90, 0);
        }
        else if (name.Equals("movingSuitcaseOpen"))
        {
            movingSuitcase.transform.position = new Vector3(0, -2, 4);
            movingSuitcase.transform.localEulerAngles = new Vector3(270, 0, 0);
            movingSuitcase.transform.Find("Bottom").localEulerAngles = new Vector3(0, 270, 0);
            movingSuitcase.transform.Find("Front").localEulerAngles = new Vector3(0, 0, 0);
            movingSuitcase.transform.Find("Main_pocket").localEulerAngles = new Vector3(0, 0, 0);
        }
    }

    public void showHideSuitcase(string name)
    {
        movingSuitcase.SetActive(name.Equals("movingSuitcase"));
        layingSuitcase.SetActive(name.Equals("layingSuitcase"));
    }

    public void showHideFunctionalities(string key)
    {
        foreach (GameObject i in fList)
        {
            if (i)
            {
                i.SetActive(key.Equals("show"));
            }
        }
        foreach (GameObject i in f7List)
        {
            if (i)
            {
                i.SetActive(key.Equals("show7"));
            }
        }
    }

    // 1. dual-access entry
    // 2. zipper expansion
    // 3. built-in usb port
    // 4. dual-coil zippers
    // 5. built-in TSA lock
    // 6. Gusseted pocket
    // 7. main compartment
    public void zoomFunctionalities(string key)
    {

        int index = int.Parse(key);
        //fList[index - 1].SetActive(true);

        if (index == 1)
        {
            showHideSuitcase("movingSuitcase");
            resetCamera("movingSuitcase");
            showHideFunctionalities("show");

            cameraScript.isEnabled = true;
            openSuitcaseScript.isTriggered = true;
        }
        else if (index == 2)
        {
            showHideSuitcase("movingSuitcase");
            resetCamera("movingSuitcase");
            showHideFunctionalities("show");

            cameraScript.isEnabled = false;
            zoomScript.zoomInObject = fList[1];
            zoomScript.isTriggered = true;
        }
        else if (index == 3)
        {
            showHideSuitcase("movingSuitcase");
            resetCamera("movingSuitcase");
            showHideFunctionalities("show");

            cameraScript.isEnabled = false;
            zoomScript.zoomInObject = fList[2];
            zoomScript.isTriggered = true;
        }
        else if (index == 4)
        {
            showHideSuitcase("movingSuitcase");
            resetCamera("movingSuitcase");
            showHideFunctionalities("show");

            cameraScript.isEnabled = false;
            zoomScript.zoomInObject = fList[3];
            zoomScript.isTriggered = true;
            rotateFrontScript.isTriggered = true;
        }
        else if (index == 5)
        {
            showHideSuitcase("movingSuitcase");
            resetCamera("movingSuitcase");
            showHideFunctionalities("show");

            cameraScript.reset(new Vector3(0, 0, 300));
            cameraScript.isEnabled = false;
            zoomScript.zoomInObject = fList[4];
            zoomScript.isTriggered = true;
            rotateFrontScript.isTriggered = true;
        }
        else if (index == 6)
        {
            showHideSuitcase("movingSuitcase");
            resetCamera("movingSuitcase");
            showHideFunctionalities("show");

            cameraScript.isEnabled = false;
            zoomScript.zoomInObject = fList[5];
            zoomScript.isTriggered = true;
            rotateFrontScript.isTriggered = true;
        }
        else if (index == 7)
        {
            showHideSuitcase("movingSuitcase");
            resetCamera("movingSuitcaseOpenPre");
            showHideFunctionalities("show7");

            cameraScript.isEnabled = true;
            layDownSuitcase.isTriggered = true;
        }
        else if (index == 71)
        {
            showHideSuitcase("movingSuitcase");
            resetCamera("movingSuitcaseOpen");
            showHideFunctionalities("show7");

            cameraScript.isEnabled = false;
            //zoomScript.zoomInObject = f7List[1];
            //zoomScript.isTriggered = true;
        }
        else if (index == 72)
        {
            showHideSuitcase("movingSuitcase");
            resetCamera("movingSuitcaseOpen");
            showHideFunctionalities("show7");

            cameraScript.isEnabled = false;
            zoomScript.zoomInObject = f7List[1];
            zoomScript.isTriggered = true;
        }
        else if (index == 73)
        {
            showHideSuitcase("movingSuitcase");
            resetCamera("movingSuitcaseOpen");
            showHideFunctionalities("show7");

            cameraScript.isEnabled = false;
            zoomScript.zoomInObject = f7List[2];
            zoomScript.isTriggered = true;
        }
        else if (index == 74)
        {
            showHideSuitcase("movingSuitcase");
            resetCamera("movingSuitcaseOpen");
            showHideFunctionalities("show7");

            cameraScript.isEnabled = false;
            zoomScript.zoomInObject = f7List[3];
            zoomScript.isTriggered = true;
        }
        else if (index == 8)
        {
            showHideSuitcase("layingSuitcase");
            resetCamera("layingSuitcase");
            //showHideFunctionalities("show");

            cameraScript.isEnabled = false;
            //zoomScript.zoomInObject = fList[7];
            //zoomScript.isTriggered = true;
            flyHanger.isTriggered = true;
        }
        else if (index == 9)
        {
            showHideSuitcase("movingSuitcase");
            resetCamera("movingSuitcase");
            showHideFunctionalities("show");

            cameraScript.isEnabled = false;
            zoomScript.zoomInObject = fList[8];
            zoomScript.isTriggered = true;
            rotateFrontScript.isTriggered = true;
            //pullHandle.isTriggered = true;
        }
        else if (index == 10)
        {
            showHideSuitcase("movingSuitcase");
            resetCamera("movingSuitcase");
            showHideFunctionalities("show");

            cameraScript.isEnabled = false;
            zoomScript.zoomInObject = fList[9];
            zoomScript.isTriggered = true;
            //rotateFrontScript.isTriggered = true;
            //pullHandle.isTriggered = true;
        }
        else if (index == 11)
        {
            showHideSuitcase("movingSuitcase");
            resetCamera("movingSuitcase");
            showHideFunctionalities("show");

            cameraScript.isEnabled = false;
            zoomScript.zoomInObject = fList[10];
            zoomScript.isTriggered = true;
            rotateFrontScript.isTriggered = true;
            //pullHandle.isTriggered = true;
        }
        else if (index == 12)
        {
            showHideSuitcase("movingSuitcase");
            resetCamera("movingSuitcaseBack");
            showHideFunctionalities("show");

            cameraScript.isEnabled = false;
            zoomScript.zoomInObject = fList[11];
            zoomScript.isTriggered = true;
            //rotateFrontScript.isTriggered = true;
            pullHandle.isTriggered = true;
        }
    }


}
