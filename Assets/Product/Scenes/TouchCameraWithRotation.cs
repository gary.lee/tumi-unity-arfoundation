﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchCameraWithRotation : MonoBehaviour
{
    public Camera mainCamera;
    public GameObject rotationPlate;
    public GameObject rotationOffsetPlate;

    public float minFoV = 15;
    public float maxFoV = 55;

    public bool isEnabled = true;

    private bool isRotationOffseted = true;

    Vector2?[] oldTouchPositions = {
        null,
        null
    };
    Vector2 oldTouchVector;
    float oldTouchDistance;

    public void reset(Vector3? targetV3)
    {
        rotationPlate.transform.localEulerAngles = Vector3.zero;
        rotationOffsetPlate.transform.localEulerAngles = targetV3 != null ? (Vector3)targetV3 : Vector3.zero;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!isEnabled) return;

        if (Input.touchCount == 0)
        {
            oldTouchPositions[0] = null;
            oldTouchPositions[1] = null;

            if (!isRotationOffseted)
            {
                isRotationOffseted = true;
                rotationOffsetPlate.transform.localEulerAngles = rotationPlate.transform.eulerAngles;
                rotationPlate.transform.localEulerAngles = Vector3.zero;
                //Debug.Log(rotationOffsetPlate.transform.eulerAngles);
                //Debug.Log(rotationPlate.transform.localEulerAngles);

            }
        }
        else if (Input.touchCount == 1)
        {
            if (oldTouchPositions[0] == null || oldTouchPositions[1] != null)
            {
                oldTouchPositions[0] = Input.GetTouch(0).position;
                oldTouchPositions[1] = null;
            }
            else
            {
                Vector2 newTouchPosition = Input.GetTouch(0).position;
                Vector3 rotationV3 = (Vector3)((oldTouchPositions[0] - newTouchPosition) * mainCamera.orthographicSize / mainCamera.pixelHeight * 25f);

                rotationPlate.transform.RotateAround(Vector3.zero, new Vector3(0, 1, 0), rotationV3.x);
                rotationPlate.transform.RotateAround(Vector3.zero, new Vector3(0, 0, 1), -rotationV3.y);
                oldTouchPositions[0] = newTouchPosition;
                isRotationOffseted = false;
            }
        }
        else
        {
            if (oldTouchPositions[1] == null)
            {
                oldTouchPositions[0] = Input.GetTouch(0).position;
                oldTouchPositions[1] = Input.GetTouch(1).position;
                oldTouchVector = (Vector2)(oldTouchPositions[0] - oldTouchPositions[1]);
                oldTouchDistance = oldTouchVector.magnitude;
            }
            else
            {
                Vector2 screen = new Vector2(mainCamera.pixelWidth, mainCamera.pixelHeight);

                Vector2[] newTouchPositions = {
                    Input.GetTouch(0).position,
                    Input.GetTouch(1).position
                };
                Vector2 newTouchVector = newTouchPositions[0] - newTouchPositions[1];
                float newTouchDistance = newTouchVector.magnitude;

                float fieldOfView = mainCamera.fieldOfView - (newTouchDistance - oldTouchDistance) * 0.2f;
                if (fieldOfView < minFoV) fieldOfView = minFoV;
                else if (fieldOfView > maxFoV) fieldOfView = maxFoV;
                mainCamera.fieldOfView = fieldOfView;

                oldTouchPositions[0] = newTouchPositions[0];
                oldTouchPositions[1] = newTouchPositions[1];
                oldTouchVector = newTouchVector;
                oldTouchDistance = newTouchDistance;
            }
        }
    }
}
