﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zoom : MonoBehaviour
{

    public Camera mainCamera;
    public GameObject zoomInObject;
    public bool isTriggered = false;
    public float initFieldOfView = 55.0f;
    public float targetFieldOfView = 20.0f;
    public int nStep = 20;


    public bool isTriggering = false;

    // dev
    public string trigger = "z";


    private int cameraNR = 20;
    private int cameraRCount = 0;
    private float step = 0;

    private void Start()
    {
        this.cameraNR = this.nStep;
    }

    void Update()
    {
        if (Input.GetKey(trigger))
        {
            this.isTriggered = true;
        }

        if (this.isTriggered)
        {
            if (!this.isTriggering)
            {
                if (this.initFieldOfView == 0)
                {
                    this.initFieldOfView = mainCamera.fieldOfView;
                }
                this.isTriggering = true;
                this.cameraRCount = 0;
                mainCamera.transform.LookAt(zoomInObject.transform);
                this.step = (this.targetFieldOfView - this.initFieldOfView) / this.nStep;
                mainCamera.fieldOfView = this.initFieldOfView;
                //Debug.Log(zoomInObject.transform);
            }
            this.isTriggered = !this.action();
            if (!this.isTriggered)
            {
                this.isTriggering = false;
                mainCamera.fieldOfView = this.targetFieldOfView;
            }
        }
    }

    bool action()
    {
        bool isFinished = true;
        if (this.cameraRCount < this.cameraNR)
        {
            isFinished = false;
            mainCamera.transform.LookAt(zoomInObject.transform);
            mainCamera.fieldOfView = this.initFieldOfView + this.cameraRCount * this.step;
            this.cameraRCount += 1;
        }
        return isFinished;
    }
}
