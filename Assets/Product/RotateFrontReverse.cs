﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateFrontReverse : MonoBehaviour
{
    public bool isTriggered = false;


    public bool isTriggering = false;
    private Vector3 thisInitV3 = new Vector3(0, 90, 0);
    private Vector3 thisTargetV3 = new Vector3(0, 0, 0);
    private Vector3 thisRotationV3 = new Vector3(0, -1, 0);
    private int thisNR = 90;
    private int thisRCount = 0;


    void Update()
    {
        if (Input.GetKey("s"))
        {
            this.isTriggered = true;
        }

        if (this.isTriggered)
        {
            if (!this.isTriggering)
            {
                this.isTriggering = true;
                this.thisRCount = 0;
                this.transform.localEulerAngles = this.thisInitV3;
            }
            this.isTriggered = !this.action();
            if (!this.isTriggered)
            {
                this.isTriggering = false;
                this.transform.localEulerAngles = this.thisTargetV3;
            }
        }
    }

    bool action()
    {
        bool isFinished = true;
        if (this.thisRCount < this.thisNR)
        {
            isFinished = false;
            this.transform.Rotate(
                thisRotationV3,
                Space.Self
            );
            this.thisRCount += 1;
        }
        return isFinished;
    }
}
