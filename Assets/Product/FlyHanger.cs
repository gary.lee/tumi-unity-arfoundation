﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyHanger : MonoBehaviour
{
    public GameObject handle;

    public bool isTriggered = false;


    public bool isTriggering = false;
    private Vector3 handleInitV3 = new Vector3(3.049517f, -1.774164f, 0);
    private Vector3 handleTargetV3 = new Vector3(7.07f, 1.52f, 0);
    private Vector3 handleTranslateV3 = new Vector3(0.20102415f, 0.1647082f, 0);
    private int handleNR = 20;
    private int handleRCount = 0;


    void Update()
    {
        if (Input.GetKey("u"))
        {
            this.isTriggered = true;
        }

        if (this.isTriggered)
        {
            if (!this.isTriggering)
            {
                this.isTriggering = true;
                this.handleRCount = 0;
                handle.transform.localPosition = this.handleInitV3;
            }
            this.isTriggered = !this.action();
            if (!this.isTriggered)
            {
                this.isTriggering = false;
                handle.transform.localPosition = this.handleTargetV3;
            }
        }
    }

    bool action()
    {
        bool isFinished = true;
        if (this.handleRCount < this.handleNR)
        {
            isFinished = false;
            handle.transform.localPosition = this.handleInitV3 + this.handleTranslateV3 * this.handleRCount;
            this.handleRCount += 1;
        }
        return isFinished;
    }
}
