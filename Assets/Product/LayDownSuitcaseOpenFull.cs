﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayDownSuitcaseOpenFull : MonoBehaviour
{
    public GameObject front;
    public GameObject bottom;
    public GameObject mainPocket;

    public bool isTriggered = false;


    public bool isTriggering = false;
    private Vector3 thisInitV3 = new Vector3(0, 0, 0);
    private Vector3 frontInitV3 = new Vector3(0, 90, 0);
    private Vector3 bottomInitV3 = new Vector3(0, 270, 0);
    private Vector3 mainPocketInitV3 = new Vector3(0, 90, 0);
    private Vector3 thisTargetV3 = new Vector3(-90, 0, 0);
    private Vector3 frontTargetV3 = new Vector3(0, 270, 0);
    private Vector3 bottomTargetV3 = new Vector3(0, 270, 0);
    private Vector3 mainPocketTargetV3 = new Vector3(0, 0, 0);
    private Vector3 thisRotationV3 = new Vector3(-1, 0, 0);
    private Vector3 frontRotationV3 = new Vector3(0, -2, 0);
    private Vector3 bottomRotationV3 = new Vector3(0, 0, 0);
    private Vector3 mainPocketRotationtV3 = new Vector3(0, -1, 0);
    private int thisNR = 90;
    private int frontNR = 90;
    private int bottomNR = 0;
    private int mainPocketNR = 90;
    private int thisRCount = 0;
    private int frontRCount = 0;
    private int bottomRCount = 0;
    private int mainPocketRCount = 0;


    void Update()
    {
        if (Input.GetKey("k"))
        {
            this.isTriggered = true;
        }

        if (this.isTriggered)
        {
            if (!this.isTriggering)
            {
                this.isTriggering = true;
                this.thisRCount = 0;
                this.frontRCount = 0;
                this.bottomRCount = 0;
                this.mainPocketRCount = 0;
                this.transform.localEulerAngles = this.thisInitV3;
                front.transform.localEulerAngles = this.frontInitV3;
                bottom.transform.localEulerAngles = this.bottomInitV3;
                mainPocket.transform.localEulerAngles = this.mainPocketInitV3;
            }
            this.isTriggered = !this.action();
            if (!this.isTriggered)
            {
                this.isTriggering = false;
                this.transform.localEulerAngles = this.thisTargetV3;
                front.transform.localEulerAngles = this.frontTargetV3;
                bottom.transform.localEulerAngles = this.bottomTargetV3;
                mainPocket.transform.localEulerAngles = this.mainPocketTargetV3;
            }
        }
    }

    bool action()
    {
        bool isFinished = true;
        if (this.thisRCount < this.thisNR)
        {
            isFinished = false;
            this.transform.Rotate(
                thisRotationV3,
                Space.Self
            );
            this.thisRCount += 1;
        }

        if (this.frontRCount < this.frontNR)
        {
            isFinished = false;
            front.transform.Rotate(
                frontRotationV3,
                Space.Self
            );
            this.frontRCount += 1;
        }


        if (this.bottomRCount < this.bottomNR)
        {
            isFinished = false;
            bottom.transform.Rotate(
                bottomRotationV3,
                Space.Self
            );
            this.bottomRCount += 1;
        }

        if (this.mainPocketRCount < this.mainPocketNR)
        {
            isFinished = false;
            mainPocket.transform.Rotate(
                mainPocketRotationtV3,
                Space.Self
            );
            this.mainPocketRCount += 1;
        }
        return isFinished;
    }
}
