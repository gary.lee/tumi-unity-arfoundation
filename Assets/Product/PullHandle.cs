﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PullHandle : MonoBehaviour
{
    public GameObject handle;

    public bool isTriggered = false;


    public bool isTriggering = false;
    private Vector3 handleInitV3 = new Vector3(-0.02981573f, 0.012f, -0.04882242f);
    private Vector3 handleTargetV3 = new Vector3(-0.02981573f, 0.04f, -0.04882242f);
    private Vector3 handleTranslateV3 = new Vector3(0, 0.0014f, 0);
    private int handleNR = 20;
    private int handleRCount = 0;


    void Update()
    {
        if (Input.GetKey("h"))
        {
            this.isTriggered = true;
        }

        if (this.isTriggered)
        {
            if (!this.isTriggering)
            {
                this.isTriggering = true;
                this.handleRCount = 0;
                handle.transform.localPosition = this.handleInitV3;
            }
            this.isTriggered = !this.action();
            if (!this.isTriggered)
            {
                this.isTriggering = false;
                handle.transform.localPosition = this.handleTargetV3;
            }
        }
    }

    bool action()
    {
        bool isFinished = true;
        if (this.handleRCount < this.handleNR)
        {
            isFinished = false;
            handle.transform.localPosition = this.handleInitV3 + this.handleTranslateV3 * this.handleRCount;
            this.handleRCount += 1;
        }
        return isFinished;
    }
}
