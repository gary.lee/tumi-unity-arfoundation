﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ScreenCapture : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void doScreenShot(string para)
    {
        NativeToolkit.SaveScreenshot("TumiScreen-" + randomId());
    }

    const string glyphs = "abcdefghijklmnopqrstuvwxyz0123456789"; //add the characters you want
    const int minCharAmount = 6;
    const int maxCharAmount = 6;

    string randomId()
    {
        string result = "";
        int charAmount = Random.Range(minCharAmount, maxCharAmount); //set those to the minimum and maximum length of your string
        for (int i = 0; i < charAmount; i++)
        {
            result+= glyphs[Random.Range(0, glyphs.Length)];
        }

        return result;
    }


    void despawnCelebrity(string para)
    {
        ARPlaneManager planeMan = (ARPlaneManager)FindObjectOfType(typeof(ARPlaneManager));
        if (planeMan != null)
        {
            List<ARPlane> s_Planes = new List<ARPlane>();

            planeMan.GetAllPlanes(s_Planes);
            foreach (var plane in s_Planes)
            {
                plane.gameObject.SetActive(true);
            }

            planeMan.planePrefab.SetActive(true);
        }

        PlaceOnPlane placePlane = (PlaceOnPlane)FindObjectOfType(typeof(PlaceOnPlane));
        if (placePlane != null)
        {
            Destroy(placePlane.spawnedObject);
            placePlane.spawnedObject = null;
        }
    }
}
