﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.XR;
using UnityEngine.XR.ARFoundation;

/// <summary>
/// Listens for touch events and performs an AR raycast from the screen touch point.
/// AR raycasts will only hit detected trackables like feature points and planes.
/// 
/// If a raycast hits a trackable, the <see cref="placedPrefab"/> is instantiated
/// and moved to the hit position.
/// </summary>
[RequireComponent(typeof(ARSessionOrigin))]
public class PlaceOnPlane : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Instantiates this prefab on a plane at the touch location.")]
    GameObject m_PlacedPrefab;

    /// <summary>
    /// The prefab to instantiate on touch.
    /// </summary>
    public GameObject placedPrefab
    {
        get { return m_PlacedPrefab; }
        set { m_PlacedPrefab = value; }
    }
    
    /// <summary>
    /// The object instantiated as a result of a successful raycast intersection with a plane.
    /// </summary>
    public GameObject spawnedObject { get; set; }

    void Awake()
    {
        m_SessionOrigin = GetComponent<ARSessionOrigin>();
    }



    void handlePinchZoom()
    {
        Touch touchZero = Input.GetTouch(0);
        Touch touchOne = Input.GetTouch(1);

        // Find the position in the previous frame of each touch.
        Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
        Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

        // Find the magnitude of the vector (the distance) between the touches in each frame.
        float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
        float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

        // Find the difference in the distances between each frame.
        float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
        float normalizedDiff = deltaMagnitudeDiff * -0.0000005f;

        if (spawnedObject != null)
        {
            Vector3 result = spawnedObject.transform.localScale;
            result += new Vector3(normalizedDiff, normalizedDiff, normalizedDiff);
            result.x = Mathf.Max(0.0f, result.x);
            result.y = Mathf.Max(0.0f, result.y);
            result.z = Mathf.Max(0.0f, result.z);

            spawnedObject.transform.localScale = result;
        }
    }

    void Update()
    {
        if (Input.touchCount == 0)
            return;


        // Handle Pinch Zoom
        if (Input.touchCount > 1)
        {
            handlePinchZoom();
            return;
        }

        var touch = Input.GetTouch(0);

        // Handle Rotation
        if (touch.phase == TouchPhase.Moved)
        {
            if (spawnedObject != null)
            {
                spawnedObject.transform.Rotate(0, touch.deltaPosition.x * 1.0f, 0, Space.World);
            }
            return;
        }

        // Handle spawning object
        if (spawnedObject != null)
        {
            return;
        }

        if (m_SessionOrigin.Raycast(touch.position, s_Hits, TrackableType.PlaneWithinPolygon))
        {
            // Raycast hits are sorted by distance, so the first one
            // will be the closest hit.
            var hitPose = s_Hits[0].pose;

            if (spawnedObject == null)
            {
                spawnedObject = Instantiate(m_PlacedPrefab, hitPose.position, hitPose.rotation);
            }
            else
            {
                spawnedObject.transform.position = hitPose.position;
            }
        }


        // Hide ARPlaneManager when spawned
        ARPlaneManager planeMan = (ARPlaneManager)FindObjectOfType(typeof(ARPlaneManager));
        if (planeMan != null)
        {
            List<ARPlane> s_Planes = new List<ARPlane>();

            planeMan.GetAllPlanes(s_Planes);
            foreach (var plane in s_Planes) { 
                plane.gameObject.SetActive(false);
            }

            planeMan.planePrefab.SetActive(false);
        }
    }

    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    ARSessionOrigin m_SessionOrigin;
}
