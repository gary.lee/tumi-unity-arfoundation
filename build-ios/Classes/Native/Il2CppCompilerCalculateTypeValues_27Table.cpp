﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ARWorldMapController
struct ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737;
// HoloVideoObject
struct HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB;
// HoloVideoObject/OnEndOfStream
struct OnEndOfStream_t6ABE2224F14F21D78185646112D85DE309AA9091;
// HoloVideoObject/OnFatalErrorEvent
struct OnFatalErrorEvent_tC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67;
// HoloVideoObject/OnFrameInfoEvent
struct OnFrameInfoEvent_t254A414B4783E99BE1CDD836552B5BEE9BB14C3F;
// HoloVideoObject/OnOpenEvent
struct OnOpenEvent_t9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1;
// HoloVideoObject/OnRenderEvent
struct OnRenderEvent_tEBC1B611E970CED1D3F6AE62BCC833366AAC60F6;
// LightEstimation
struct LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319;
// MakeAppearOnPlane
struct MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3;
// SVFUnityPluginInterop
struct SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<HVConductor/HVSequence>
struct List_1_t72FCE2C4A1716A632723FDF54D82B4B2F8C3750F;
// System.Collections.Generic.List`1<HoloVideoObject>
struct List_1_t5193736D94FB3C884A7F72E642A433324BBB8C5C;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_t138C3A87AA33BCF0AE9B7978FD403BF0464EBC8B;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>
struct List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B;
// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit>
struct List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.BinaryReader
struct BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969;
// System.IO.StringReader
struct StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12;
// System.Nullable`1<UnityEngine.Vector2>[]
struct Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.AudioListener
struct AudioListener_tE3E1467B84A4AFD509947B44A7C8ACFB67FF2099;
// UnityEngine.BoxCollider
struct BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA;
// UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC;
// UnityEngine.Experimental.XR.XRSessionSubsystem
struct XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Light
struct Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C;
// UnityEngine.Logger
struct Logger_tEB67FD7450076B84B97F22DBE8B2911FC4FBC35F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.MeshFilter
struct MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0;
// UnityEngine.MeshRenderer
struct MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED;
// UnityEngine.Mesh[]
struct MeshU5BU5D_tDD9C723AA6F0225B35A93D871CDC2CEFF7F8CB89;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Dropdown
struct Dropdown_tF6331401084B1213CAB10587A6EC81461501930F;
// UnityEngine.UI.RawImage
struct RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.XR.ARFoundation.ARPlane
struct ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E;
// UnityEngine.XR.ARFoundation.ARPlaneManager
struct ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A;
// UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer
struct ARPlaneMeshVisualizer_t1CAC012DACAA7D8D30B94247B8762D71D2624923;
// UnityEngine.XR.ARFoundation.ARSession
struct ARSession_tFD6F1BD76D4C003B8141D9B6255B904D8C5036AB;
// UnityEngine.XR.ARFoundation.ARSessionOrigin
struct ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CLOADU3ED__28_T5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D_H
#define U3CLOADU3ED__28_T5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARWorldMapController_<Load>d__28
struct  U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D  : public RuntimeObject
{
public:
	// System.Int32 ARWorldMapController_<Load>d__28::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ARWorldMapController_<Load>d__28::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ARWorldMapController ARWorldMapController_<Load>d__28::<>4__this
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 * ___U3CU3E4__this_2;
	// UnityEngine.Experimental.XR.XRSessionSubsystem ARWorldMapController_<Load>d__28::<sessionSubsystem>5__2
	XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F * ___U3CsessionSubsystemU3E5__2_3;
	// System.Int32 ARWorldMapController_<Load>d__28::<bytesPerFrame>5__3
	int32_t ___U3CbytesPerFrameU3E5__3_4;
	// System.Int64 ARWorldMapController_<Load>d__28::<bytesRemaining>5__4
	int64_t ___U3CbytesRemainingU3E5__4_5;
	// System.IO.BinaryReader ARWorldMapController_<Load>d__28::<binaryReader>5__5
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * ___U3CbinaryReaderU3E5__5_6;
	// System.Collections.Generic.List`1<System.Byte> ARWorldMapController_<Load>d__28::<allBytes>5__6
	List_1_t138C3A87AA33BCF0AE9B7978FD403BF0464EBC8B * ___U3CallBytesU3E5__6_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CU3E4__this_2)); }
	inline ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CsessionSubsystemU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CsessionSubsystemU3E5__2_3)); }
	inline XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F * get_U3CsessionSubsystemU3E5__2_3() const { return ___U3CsessionSubsystemU3E5__2_3; }
	inline XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F ** get_address_of_U3CsessionSubsystemU3E5__2_3() { return &___U3CsessionSubsystemU3E5__2_3; }
	inline void set_U3CsessionSubsystemU3E5__2_3(XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F * value)
	{
		___U3CsessionSubsystemU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsessionSubsystemU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CbytesPerFrameU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CbytesPerFrameU3E5__3_4)); }
	inline int32_t get_U3CbytesPerFrameU3E5__3_4() const { return ___U3CbytesPerFrameU3E5__3_4; }
	inline int32_t* get_address_of_U3CbytesPerFrameU3E5__3_4() { return &___U3CbytesPerFrameU3E5__3_4; }
	inline void set_U3CbytesPerFrameU3E5__3_4(int32_t value)
	{
		___U3CbytesPerFrameU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3CbytesRemainingU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CbytesRemainingU3E5__4_5)); }
	inline int64_t get_U3CbytesRemainingU3E5__4_5() const { return ___U3CbytesRemainingU3E5__4_5; }
	inline int64_t* get_address_of_U3CbytesRemainingU3E5__4_5() { return &___U3CbytesRemainingU3E5__4_5; }
	inline void set_U3CbytesRemainingU3E5__4_5(int64_t value)
	{
		___U3CbytesRemainingU3E5__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CbinaryReaderU3E5__5_6() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CbinaryReaderU3E5__5_6)); }
	inline BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * get_U3CbinaryReaderU3E5__5_6() const { return ___U3CbinaryReaderU3E5__5_6; }
	inline BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 ** get_address_of_U3CbinaryReaderU3E5__5_6() { return &___U3CbinaryReaderU3E5__5_6; }
	inline void set_U3CbinaryReaderU3E5__5_6(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * value)
	{
		___U3CbinaryReaderU3E5__5_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbinaryReaderU3E5__5_6), value);
	}

	inline static int32_t get_offset_of_U3CallBytesU3E5__6_7() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CallBytesU3E5__6_7)); }
	inline List_1_t138C3A87AA33BCF0AE9B7978FD403BF0464EBC8B * get_U3CallBytesU3E5__6_7() const { return ___U3CallBytesU3E5__6_7; }
	inline List_1_t138C3A87AA33BCF0AE9B7978FD403BF0464EBC8B ** get_address_of_U3CallBytesU3E5__6_7() { return &___U3CallBytesU3E5__6_7; }
	inline void set_U3CallBytesU3E5__6_7(List_1_t138C3A87AA33BCF0AE9B7978FD403BF0464EBC8B * value)
	{
		___U3CallBytesU3E5__6_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CallBytesU3E5__6_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADU3ED__28_T5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D_H
#ifndef U3CFILLUNITYBUFFERSU3ED__48_T43B9E283B7914A96EDF86C9C3B4E39161E387017_H
#define U3CFILLUNITYBUFFERSU3ED__48_T43B9E283B7914A96EDF86C9C3B4E39161E387017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloVideoObject_<FillUnityBuffers>d__48
struct  U3CFillUnityBuffersU3Ed__48_t43B9E283B7914A96EDF86C9C3B4E39161E387017  : public RuntimeObject
{
public:
	// System.Int32 HoloVideoObject_<FillUnityBuffers>d__48::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloVideoObject_<FillUnityBuffers>d__48::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloVideoObject HoloVideoObject_<FillUnityBuffers>d__48::<>4__this
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFillUnityBuffersU3Ed__48_t43B9E283B7914A96EDF86C9C3B4E39161E387017, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFillUnityBuffersU3Ed__48_t43B9E283B7914A96EDF86C9C3B4E39161E387017, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFillUnityBuffersU3Ed__48_t43B9E283B7914A96EDF86C9C3B4E39161E387017, ___U3CU3E4__this_2)); }
	inline HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFILLUNITYBUFFERSU3ED__48_T43B9E283B7914A96EDF86C9C3B4E39161E387017_H
#ifndef JSON_TB4C7A53B66AB0AFB87D47818778AF9AB10586B7B_H
#define JSON_TB4C7A53B66AB0AFB87D47818778AF9AB10586B7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniJSON.Json
struct  Json_tB4C7A53B66AB0AFB87D47818778AF9AB10586B7B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_TB4C7A53B66AB0AFB87D47818778AF9AB10586B7B_H
#ifndef PARSER_T3B57A69142237AEB04F76D09DD2581735AE34413_H
#define PARSER_T3B57A69142237AEB04F76D09DD2581735AE34413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniJSON.Json_Parser
struct  Parser_t3B57A69142237AEB04F76D09DD2581735AE34413  : public RuntimeObject
{
public:
	// System.IO.StringReader MiniJSON.Json_Parser::json
	StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 * ___json_1;

public:
	inline static int32_t get_offset_of_json_1() { return static_cast<int32_t>(offsetof(Parser_t3B57A69142237AEB04F76D09DD2581735AE34413, ___json_1)); }
	inline StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 * get_json_1() const { return ___json_1; }
	inline StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 ** get_address_of_json_1() { return &___json_1; }
	inline void set_json_1(StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 * value)
	{
		___json_1 = value;
		Il2CppCodeGenWriteBarrier((&___json_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSER_T3B57A69142237AEB04F76D09DD2581735AE34413_H
#ifndef SERIALIZER_TBBDA1DA8B8B2CC192031334142175F9F822049F4_H
#define SERIALIZER_TBBDA1DA8B8B2CC192031334142175F9F822049F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniJSON.Json_Serializer
struct  Serializer_tBBDA1DA8B8B2CC192031334142175F9F822049F4  : public RuntimeObject
{
public:
	// System.Text.StringBuilder MiniJSON.Json_Serializer::builder
	StringBuilder_t * ___builder_0;

public:
	inline static int32_t get_offset_of_builder_0() { return static_cast<int32_t>(offsetof(Serializer_tBBDA1DA8B8B2CC192031334142175F9F822049F4, ___builder_0)); }
	inline StringBuilder_t * get_builder_0() const { return ___builder_0; }
	inline StringBuilder_t ** get_address_of_builder_0() { return &___builder_0; }
	inline void set_builder_0(StringBuilder_t * value)
	{
		___builder_0 = value;
		Il2CppCodeGenWriteBarrier((&___builder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_TBBDA1DA8B8B2CC192031334142175F9F822049F4_H
#ifndef SVFPLAYBACKSTATEHELPER_T38FB181A4A2C8E48930305CA86FC64E2DF8ED3EE_H
#define SVFPLAYBACKSTATEHELPER_T38FB181A4A2C8E48930305CA86FC64E2DF8ED3EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVFPlaybackStateHelper
struct  SVFPlaybackStateHelper_t38FB181A4A2C8E48930305CA86FC64E2DF8ED3EE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVFPLAYBACKSTATEHELPER_T38FB181A4A2C8E48930305CA86FC64E2DF8ED3EE_H
#ifndef SVFREADERSTATEHELPER_TF26BD4D553776D5DB620A2325BF3625E57D30684_H
#define SVFREADERSTATEHELPER_TF26BD4D553776D5DB620A2325BF3625E57D30684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVFReaderStateHelper
struct  SVFReaderStateHelper_tF26BD4D553776D5DB620A2325BF3625E57D30684  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVFREADERSTATEHELPER_TF26BD4D553776D5DB620A2325BF3625E57D30684_H
#ifndef SVFUNITYPLUGININTEROP_T0499F937D90ED0EB487AFB71739A0D17BA0717F8_H
#define SVFUNITYPLUGININTEROP_T0499F937D90ED0EB487AFB71739A0D17BA0717F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVFUnityPluginInterop
struct  SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8  : public RuntimeObject
{
public:
	// UnityEngine.Logger SVFUnityPluginInterop::logger
	Logger_tEB67FD7450076B84B97F22DBE8B2911FC4FBC35F * ___logger_2;
	// System.Int32 SVFUnityPluginInterop::instanceId
	int32_t ___instanceId_4;
	// System.Boolean SVFUnityPluginInterop::isInstanceDisposed
	bool ___isInstanceDisposed_5;

public:
	inline static int32_t get_offset_of_logger_2() { return static_cast<int32_t>(offsetof(SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8, ___logger_2)); }
	inline Logger_tEB67FD7450076B84B97F22DBE8B2911FC4FBC35F * get_logger_2() const { return ___logger_2; }
	inline Logger_tEB67FD7450076B84B97F22DBE8B2911FC4FBC35F ** get_address_of_logger_2() { return &___logger_2; }
	inline void set_logger_2(Logger_tEB67FD7450076B84B97F22DBE8B2911FC4FBC35F * value)
	{
		___logger_2 = value;
		Il2CppCodeGenWriteBarrier((&___logger_2), value);
	}

	inline static int32_t get_offset_of_instanceId_4() { return static_cast<int32_t>(offsetof(SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8, ___instanceId_4)); }
	inline int32_t get_instanceId_4() const { return ___instanceId_4; }
	inline int32_t* get_address_of_instanceId_4() { return &___instanceId_4; }
	inline void set_instanceId_4(int32_t value)
	{
		___instanceId_4 = value;
	}

	inline static int32_t get_offset_of_isInstanceDisposed_5() { return static_cast<int32_t>(offsetof(SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8, ___isInstanceDisposed_5)); }
	inline bool get_isInstanceDisposed_5() const { return ___isInstanceDisposed_5; }
	inline bool* get_address_of_isInstanceDisposed_5() { return &___isInstanceDisposed_5; }
	inline void set_isInstanceDisposed_5(bool value)
	{
		___isInstanceDisposed_5 = value;
	}
};

struct SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8_StaticFields
{
public:
	// System.Boolean SVFUnityPluginInterop::s_logstack
	bool ___s_logstack_1;

public:
	inline static int32_t get_offset_of_s_logstack_1() { return static_cast<int32_t>(offsetof(SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8_StaticFields, ___s_logstack_1)); }
	inline bool get_s_logstack_1() const { return ___s_logstack_1; }
	inline bool* get_address_of_s_logstack_1() { return &___s_logstack_1; }
	inline void set_s_logstack_1(bool value)
	{
		___s_logstack_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVFUNITYPLUGININTEROP_T0499F937D90ED0EB487AFB71739A0D17BA0717F8_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef AUDIODEVICEINFO_T105929EDE9CEF6D58A0CBF914F72C1C1B608D036_H
#define AUDIODEVICEINFO_T105929EDE9CEF6D58A0CBF914F72C1C1B608D036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioDeviceInfo
struct  AudioDeviceInfo_t105929EDE9CEF6D58A0CBF914F72C1C1B608D036 
{
public:
	// System.String AudioDeviceInfo::Name
	String_t* ___Name_0;
	// System.String AudioDeviceInfo::Id
	String_t* ___Id_1;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(AudioDeviceInfo_t105929EDE9CEF6D58A0CBF914F72C1C1B608D036, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Id_1() { return static_cast<int32_t>(offsetof(AudioDeviceInfo_t105929EDE9CEF6D58A0CBF914F72C1C1B608D036, ___Id_1)); }
	inline String_t* get_Id_1() const { return ___Id_1; }
	inline String_t** get_address_of_Id_1() { return &___Id_1; }
	inline void set_Id_1(String_t* value)
	{
		___Id_1 = value;
		Il2CppCodeGenWriteBarrier((&___Id_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of AudioDeviceInfo
struct AudioDeviceInfo_t105929EDE9CEF6D58A0CBF914F72C1C1B608D036_marshaled_pinvoke
{
	Il2CppChar ___Name_0[1024];
	Il2CppChar ___Id_1[1024];
};
// Native definition for COM marshalling of AudioDeviceInfo
struct AudioDeviceInfo_t105929EDE9CEF6D58A0CBF914F72C1C1B608D036_marshaled_com
{
	Il2CppChar ___Name_0[1024];
	Il2CppChar ___Id_1[1024];
};
#endif // AUDIODEVICEINFO_T105929EDE9CEF6D58A0CBF914F72C1C1B608D036_H
#ifndef HCAPSETTINGSINTEROP_TD574A2802911FCB0A5F59921EE9264B708ECDF4F_H
#define HCAPSETTINGSINTEROP_TD574A2802911FCB0A5F59921EE9264B708ECDF4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HCapSettingsInterop
struct  HCapSettingsInterop_tD574A2802911FCB0A5F59921EE9264B708ECDF4F 
{
public:
	// System.UInt32 HCapSettingsInterop::defaultMaxVertexCount
	uint32_t ___defaultMaxVertexCount_0;
	// System.UInt32 HCapSettingsInterop::defaultMaxIndexCount
	uint32_t ___defaultMaxIndexCount_1;

public:
	inline static int32_t get_offset_of_defaultMaxVertexCount_0() { return static_cast<int32_t>(offsetof(HCapSettingsInterop_tD574A2802911FCB0A5F59921EE9264B708ECDF4F, ___defaultMaxVertexCount_0)); }
	inline uint32_t get_defaultMaxVertexCount_0() const { return ___defaultMaxVertexCount_0; }
	inline uint32_t* get_address_of_defaultMaxVertexCount_0() { return &___defaultMaxVertexCount_0; }
	inline void set_defaultMaxVertexCount_0(uint32_t value)
	{
		___defaultMaxVertexCount_0 = value;
	}

	inline static int32_t get_offset_of_defaultMaxIndexCount_1() { return static_cast<int32_t>(offsetof(HCapSettingsInterop_tD574A2802911FCB0A5F59921EE9264B708ECDF4F, ___defaultMaxIndexCount_1)); }
	inline uint32_t get_defaultMaxIndexCount_1() const { return ___defaultMaxIndexCount_1; }
	inline uint32_t* get_address_of_defaultMaxIndexCount_1() { return &___defaultMaxIndexCount_1; }
	inline void set_defaultMaxIndexCount_1(uint32_t value)
	{
		___defaultMaxIndexCount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HCAPSETTINGSINTEROP_TD574A2802911FCB0A5F59921EE9264B708ECDF4F_H
#ifndef HVSEQUENCE_TA5A0E5C759DEDC1FE7B4B517AB3523A256C96BA1_H
#define HVSEQUENCE_TA5A0E5C759DEDC1FE7B4B517AB3523A256C96BA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HVConductor_HVSequence
struct  HVSequence_tA5A0E5C759DEDC1FE7B4B517AB3523A256C96BA1 
{
public:
	// System.Collections.Generic.List`1<HoloVideoObject> HVConductor_HVSequence::VideosToLoad
	List_1_t5193736D94FB3C884A7F72E642A433324BBB8C5C * ___VideosToLoad_0;

public:
	inline static int32_t get_offset_of_VideosToLoad_0() { return static_cast<int32_t>(offsetof(HVSequence_tA5A0E5C759DEDC1FE7B4B517AB3523A256C96BA1, ___VideosToLoad_0)); }
	inline List_1_t5193736D94FB3C884A7F72E642A433324BBB8C5C * get_VideosToLoad_0() const { return ___VideosToLoad_0; }
	inline List_1_t5193736D94FB3C884A7F72E642A433324BBB8C5C ** get_address_of_VideosToLoad_0() { return &___VideosToLoad_0; }
	inline void set_VideosToLoad_0(List_1_t5193736D94FB3C884A7F72E642A433324BBB8C5C * value)
	{
		___VideosToLoad_0 = value;
		Il2CppCodeGenWriteBarrier((&___VideosToLoad_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HVConductor/HVSequence
struct HVSequence_tA5A0E5C759DEDC1FE7B4B517AB3523A256C96BA1_marshaled_pinvoke
{
	List_1_t5193736D94FB3C884A7F72E642A433324BBB8C5C * ___VideosToLoad_0;
};
// Native definition for COM marshalling of HVConductor/HVSequence
struct HVSequence_tA5A0E5C759DEDC1FE7B4B517AB3523A256C96BA1_marshaled_com
{
	List_1_t5193736D94FB3C884A7F72E642A433324BBB8C5C * ___VideosToLoad_0;
};
#endif // HVSEQUENCE_TA5A0E5C759DEDC1FE7B4B517AB3523A256C96BA1_H
#ifndef MATRIX4X4PLUGININTEROP_T66E8E2EC5E470D261DFBA691CBC8244F001820B2_H
#define MATRIX4X4PLUGININTEROP_T66E8E2EC5E470D261DFBA691CBC8244F001820B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Matrix4x4PluginInterop
struct  Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2 
{
public:
	// System.Single Matrix4x4PluginInterop::m00
	float ___m00_0;
	// System.Single Matrix4x4PluginInterop::m01
	float ___m01_1;
	// System.Single Matrix4x4PluginInterop::m02
	float ___m02_2;
	// System.Single Matrix4x4PluginInterop::m03
	float ___m03_3;
	// System.Single Matrix4x4PluginInterop::m10
	float ___m10_4;
	// System.Single Matrix4x4PluginInterop::m11
	float ___m11_5;
	// System.Single Matrix4x4PluginInterop::m12
	float ___m12_6;
	// System.Single Matrix4x4PluginInterop::m13
	float ___m13_7;
	// System.Single Matrix4x4PluginInterop::m20
	float ___m20_8;
	// System.Single Matrix4x4PluginInterop::m21
	float ___m21_9;
	// System.Single Matrix4x4PluginInterop::m22
	float ___m22_10;
	// System.Single Matrix4x4PluginInterop::m23
	float ___m23_11;
	// System.Single Matrix4x4PluginInterop::m30
	float ___m30_12;
	// System.Single Matrix4x4PluginInterop::m31
	float ___m31_13;
	// System.Single Matrix4x4PluginInterop::m32
	float ___m32_14;
	// System.Single Matrix4x4PluginInterop::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m01_1() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m01_1)); }
	inline float get_m01_1() const { return ___m01_1; }
	inline float* get_address_of_m01_1() { return &___m01_1; }
	inline void set_m01_1(float value)
	{
		___m01_1 = value;
	}

	inline static int32_t get_offset_of_m02_2() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m02_2)); }
	inline float get_m02_2() const { return ___m02_2; }
	inline float* get_address_of_m02_2() { return &___m02_2; }
	inline void set_m02_2(float value)
	{
		___m02_2 = value;
	}

	inline static int32_t get_offset_of_m03_3() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m03_3)); }
	inline float get_m03_3() const { return ___m03_3; }
	inline float* get_address_of_m03_3() { return &___m03_3; }
	inline void set_m03_3(float value)
	{
		___m03_3 = value;
	}

	inline static int32_t get_offset_of_m10_4() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m10_4)); }
	inline float get_m10_4() const { return ___m10_4; }
	inline float* get_address_of_m10_4() { return &___m10_4; }
	inline void set_m10_4(float value)
	{
		___m10_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m12_6() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m12_6)); }
	inline float get_m12_6() const { return ___m12_6; }
	inline float* get_address_of_m12_6() { return &___m12_6; }
	inline void set_m12_6(float value)
	{
		___m12_6 = value;
	}

	inline static int32_t get_offset_of_m13_7() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m13_7)); }
	inline float get_m13_7() const { return ___m13_7; }
	inline float* get_address_of_m13_7() { return &___m13_7; }
	inline void set_m13_7(float value)
	{
		___m13_7 = value;
	}

	inline static int32_t get_offset_of_m20_8() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m20_8)); }
	inline float get_m20_8() const { return ___m20_8; }
	inline float* get_address_of_m20_8() { return &___m20_8; }
	inline void set_m20_8(float value)
	{
		___m20_8 = value;
	}

	inline static int32_t get_offset_of_m21_9() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m21_9)); }
	inline float get_m21_9() const { return ___m21_9; }
	inline float* get_address_of_m21_9() { return &___m21_9; }
	inline void set_m21_9(float value)
	{
		___m21_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m23_11() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m23_11)); }
	inline float get_m23_11() const { return ___m23_11; }
	inline float* get_address_of_m23_11() { return &___m23_11; }
	inline void set_m23_11(float value)
	{
		___m23_11 = value;
	}

	inline static int32_t get_offset_of_m30_12() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m30_12)); }
	inline float get_m30_12() const { return ___m30_12; }
	inline float* get_address_of_m30_12() { return &___m30_12; }
	inline void set_m30_12(float value)
	{
		___m30_12 = value;
	}

	inline static int32_t get_offset_of_m31_13() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m31_13)); }
	inline float get_m31_13() const { return ___m31_13; }
	inline float* get_address_of_m31_13() { return &___m31_13; }
	inline void set_m31_13(float value)
	{
		___m31_13 = value;
	}

	inline static int32_t get_offset_of_m32_14() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m32_14)); }
	inline float get_m32_14() const { return ___m32_14; }
	inline float* get_address_of_m32_14() { return &___m32_14; }
	inline void set_m32_14(float value)
	{
		___m32_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4PLUGININTEROP_T66E8E2EC5E470D261DFBA691CBC8244F001820B2_H
#ifndef SVFFILEINFO_TD86F29AD52536786393A9657E2DB669F60A32366_H
#define SVFFILEINFO_TD86F29AD52536786393A9657E2DB669F60A32366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVFFileInfo
struct  SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366 
{
public:
	// System.Boolean SVFFileInfo::hasAudio
	bool ___hasAudio_0;
	// System.UInt64 SVFFileInfo::duration100ns
	uint64_t ___duration100ns_1;
	// System.UInt32 SVFFileInfo::frameCount
	uint32_t ___frameCount_2;
	// System.UInt32 SVFFileInfo::maxVertexCount
	uint32_t ___maxVertexCount_3;
	// System.UInt32 SVFFileInfo::maxIndexCount
	uint32_t ___maxIndexCount_4;
	// System.Single SVFFileInfo::bitrateMbps
	float ___bitrateMbps_5;
	// System.Single SVFFileInfo::fileSize
	float ___fileSize_6;
	// System.Double SVFFileInfo::minX
	double ___minX_7;
	// System.Double SVFFileInfo::minY
	double ___minY_8;
	// System.Double SVFFileInfo::minZ
	double ___minZ_9;
	// System.Double SVFFileInfo::maxX
	double ___maxX_10;
	// System.Double SVFFileInfo::maxY
	double ___maxY_11;
	// System.Double SVFFileInfo::maxZ
	double ___maxZ_12;
	// System.UInt32 SVFFileInfo::fileWidth
	uint32_t ___fileWidth_13;
	// System.UInt32 SVFFileInfo::fileHeight
	uint32_t ___fileHeight_14;
	// System.Boolean SVFFileInfo::hasNormals
	bool ___hasNormals_15;

public:
	inline static int32_t get_offset_of_hasAudio_0() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___hasAudio_0)); }
	inline bool get_hasAudio_0() const { return ___hasAudio_0; }
	inline bool* get_address_of_hasAudio_0() { return &___hasAudio_0; }
	inline void set_hasAudio_0(bool value)
	{
		___hasAudio_0 = value;
	}

	inline static int32_t get_offset_of_duration100ns_1() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___duration100ns_1)); }
	inline uint64_t get_duration100ns_1() const { return ___duration100ns_1; }
	inline uint64_t* get_address_of_duration100ns_1() { return &___duration100ns_1; }
	inline void set_duration100ns_1(uint64_t value)
	{
		___duration100ns_1 = value;
	}

	inline static int32_t get_offset_of_frameCount_2() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___frameCount_2)); }
	inline uint32_t get_frameCount_2() const { return ___frameCount_2; }
	inline uint32_t* get_address_of_frameCount_2() { return &___frameCount_2; }
	inline void set_frameCount_2(uint32_t value)
	{
		___frameCount_2 = value;
	}

	inline static int32_t get_offset_of_maxVertexCount_3() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___maxVertexCount_3)); }
	inline uint32_t get_maxVertexCount_3() const { return ___maxVertexCount_3; }
	inline uint32_t* get_address_of_maxVertexCount_3() { return &___maxVertexCount_3; }
	inline void set_maxVertexCount_3(uint32_t value)
	{
		___maxVertexCount_3 = value;
	}

	inline static int32_t get_offset_of_maxIndexCount_4() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___maxIndexCount_4)); }
	inline uint32_t get_maxIndexCount_4() const { return ___maxIndexCount_4; }
	inline uint32_t* get_address_of_maxIndexCount_4() { return &___maxIndexCount_4; }
	inline void set_maxIndexCount_4(uint32_t value)
	{
		___maxIndexCount_4 = value;
	}

	inline static int32_t get_offset_of_bitrateMbps_5() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___bitrateMbps_5)); }
	inline float get_bitrateMbps_5() const { return ___bitrateMbps_5; }
	inline float* get_address_of_bitrateMbps_5() { return &___bitrateMbps_5; }
	inline void set_bitrateMbps_5(float value)
	{
		___bitrateMbps_5 = value;
	}

	inline static int32_t get_offset_of_fileSize_6() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___fileSize_6)); }
	inline float get_fileSize_6() const { return ___fileSize_6; }
	inline float* get_address_of_fileSize_6() { return &___fileSize_6; }
	inline void set_fileSize_6(float value)
	{
		___fileSize_6 = value;
	}

	inline static int32_t get_offset_of_minX_7() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___minX_7)); }
	inline double get_minX_7() const { return ___minX_7; }
	inline double* get_address_of_minX_7() { return &___minX_7; }
	inline void set_minX_7(double value)
	{
		___minX_7 = value;
	}

	inline static int32_t get_offset_of_minY_8() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___minY_8)); }
	inline double get_minY_8() const { return ___minY_8; }
	inline double* get_address_of_minY_8() { return &___minY_8; }
	inline void set_minY_8(double value)
	{
		___minY_8 = value;
	}

	inline static int32_t get_offset_of_minZ_9() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___minZ_9)); }
	inline double get_minZ_9() const { return ___minZ_9; }
	inline double* get_address_of_minZ_9() { return &___minZ_9; }
	inline void set_minZ_9(double value)
	{
		___minZ_9 = value;
	}

	inline static int32_t get_offset_of_maxX_10() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___maxX_10)); }
	inline double get_maxX_10() const { return ___maxX_10; }
	inline double* get_address_of_maxX_10() { return &___maxX_10; }
	inline void set_maxX_10(double value)
	{
		___maxX_10 = value;
	}

	inline static int32_t get_offset_of_maxY_11() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___maxY_11)); }
	inline double get_maxY_11() const { return ___maxY_11; }
	inline double* get_address_of_maxY_11() { return &___maxY_11; }
	inline void set_maxY_11(double value)
	{
		___maxY_11 = value;
	}

	inline static int32_t get_offset_of_maxZ_12() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___maxZ_12)); }
	inline double get_maxZ_12() const { return ___maxZ_12; }
	inline double* get_address_of_maxZ_12() { return &___maxZ_12; }
	inline void set_maxZ_12(double value)
	{
		___maxZ_12 = value;
	}

	inline static int32_t get_offset_of_fileWidth_13() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___fileWidth_13)); }
	inline uint32_t get_fileWidth_13() const { return ___fileWidth_13; }
	inline uint32_t* get_address_of_fileWidth_13() { return &___fileWidth_13; }
	inline void set_fileWidth_13(uint32_t value)
	{
		___fileWidth_13 = value;
	}

	inline static int32_t get_offset_of_fileHeight_14() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___fileHeight_14)); }
	inline uint32_t get_fileHeight_14() const { return ___fileHeight_14; }
	inline uint32_t* get_address_of_fileHeight_14() { return &___fileHeight_14; }
	inline void set_fileHeight_14(uint32_t value)
	{
		___fileHeight_14 = value;
	}

	inline static int32_t get_offset_of_hasNormals_15() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___hasNormals_15)); }
	inline bool get_hasNormals_15() const { return ___hasNormals_15; }
	inline bool* get_address_of_hasNormals_15() { return &___hasNormals_15; }
	inline void set_hasNormals_15(bool value)
	{
		___hasNormals_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVFFILEINFO_TD86F29AD52536786393A9657E2DB669F60A32366_H
#ifndef SVFFRAMEINFO_T53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E_H
#define SVFFRAMEINFO_T53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVFFrameInfo
struct  SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E 
{
public:
	// System.UInt64 SVFFrameInfo::frameTimestamp
	uint64_t ___frameTimestamp_0;
	// System.Double SVFFrameInfo::minX
	double ___minX_1;
	// System.Double SVFFrameInfo::minY
	double ___minY_2;
	// System.Double SVFFrameInfo::minZ
	double ___minZ_3;
	// System.Double SVFFrameInfo::maxX
	double ___maxX_4;
	// System.Double SVFFrameInfo::maxY
	double ___maxY_5;
	// System.Double SVFFrameInfo::maxZ
	double ___maxZ_6;
	// System.UInt32 SVFFrameInfo::frameId
	uint32_t ___frameId_7;
	// System.UInt32 SVFFrameInfo::vertexCount
	uint32_t ___vertexCount_8;
	// System.UInt32 SVFFrameInfo::indexCount
	uint32_t ___indexCount_9;
	// System.Int32 SVFFrameInfo::textureWidth
	int32_t ___textureWidth_10;
	// System.Int32 SVFFrameInfo::textureHeight
	int32_t ___textureHeight_11;
	// System.Boolean SVFFrameInfo::isEOS
	bool ___isEOS_12;
	// System.Boolean SVFFrameInfo::isRepeatedFrame
	bool ___isRepeatedFrame_13;
	// System.Boolean SVFFrameInfo::isKeyFrame
	bool ___isKeyFrame_14;

public:
	inline static int32_t get_offset_of_frameTimestamp_0() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___frameTimestamp_0)); }
	inline uint64_t get_frameTimestamp_0() const { return ___frameTimestamp_0; }
	inline uint64_t* get_address_of_frameTimestamp_0() { return &___frameTimestamp_0; }
	inline void set_frameTimestamp_0(uint64_t value)
	{
		___frameTimestamp_0 = value;
	}

	inline static int32_t get_offset_of_minX_1() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___minX_1)); }
	inline double get_minX_1() const { return ___minX_1; }
	inline double* get_address_of_minX_1() { return &___minX_1; }
	inline void set_minX_1(double value)
	{
		___minX_1 = value;
	}

	inline static int32_t get_offset_of_minY_2() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___minY_2)); }
	inline double get_minY_2() const { return ___minY_2; }
	inline double* get_address_of_minY_2() { return &___minY_2; }
	inline void set_minY_2(double value)
	{
		___minY_2 = value;
	}

	inline static int32_t get_offset_of_minZ_3() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___minZ_3)); }
	inline double get_minZ_3() const { return ___minZ_3; }
	inline double* get_address_of_minZ_3() { return &___minZ_3; }
	inline void set_minZ_3(double value)
	{
		___minZ_3 = value;
	}

	inline static int32_t get_offset_of_maxX_4() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___maxX_4)); }
	inline double get_maxX_4() const { return ___maxX_4; }
	inline double* get_address_of_maxX_4() { return &___maxX_4; }
	inline void set_maxX_4(double value)
	{
		___maxX_4 = value;
	}

	inline static int32_t get_offset_of_maxY_5() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___maxY_5)); }
	inline double get_maxY_5() const { return ___maxY_5; }
	inline double* get_address_of_maxY_5() { return &___maxY_5; }
	inline void set_maxY_5(double value)
	{
		___maxY_5 = value;
	}

	inline static int32_t get_offset_of_maxZ_6() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___maxZ_6)); }
	inline double get_maxZ_6() const { return ___maxZ_6; }
	inline double* get_address_of_maxZ_6() { return &___maxZ_6; }
	inline void set_maxZ_6(double value)
	{
		___maxZ_6 = value;
	}

	inline static int32_t get_offset_of_frameId_7() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___frameId_7)); }
	inline uint32_t get_frameId_7() const { return ___frameId_7; }
	inline uint32_t* get_address_of_frameId_7() { return &___frameId_7; }
	inline void set_frameId_7(uint32_t value)
	{
		___frameId_7 = value;
	}

	inline static int32_t get_offset_of_vertexCount_8() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___vertexCount_8)); }
	inline uint32_t get_vertexCount_8() const { return ___vertexCount_8; }
	inline uint32_t* get_address_of_vertexCount_8() { return &___vertexCount_8; }
	inline void set_vertexCount_8(uint32_t value)
	{
		___vertexCount_8 = value;
	}

	inline static int32_t get_offset_of_indexCount_9() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___indexCount_9)); }
	inline uint32_t get_indexCount_9() const { return ___indexCount_9; }
	inline uint32_t* get_address_of_indexCount_9() { return &___indexCount_9; }
	inline void set_indexCount_9(uint32_t value)
	{
		___indexCount_9 = value;
	}

	inline static int32_t get_offset_of_textureWidth_10() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___textureWidth_10)); }
	inline int32_t get_textureWidth_10() const { return ___textureWidth_10; }
	inline int32_t* get_address_of_textureWidth_10() { return &___textureWidth_10; }
	inline void set_textureWidth_10(int32_t value)
	{
		___textureWidth_10 = value;
	}

	inline static int32_t get_offset_of_textureHeight_11() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___textureHeight_11)); }
	inline int32_t get_textureHeight_11() const { return ___textureHeight_11; }
	inline int32_t* get_address_of_textureHeight_11() { return &___textureHeight_11; }
	inline void set_textureHeight_11(int32_t value)
	{
		___textureHeight_11 = value;
	}

	inline static int32_t get_offset_of_isEOS_12() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___isEOS_12)); }
	inline bool get_isEOS_12() const { return ___isEOS_12; }
	inline bool* get_address_of_isEOS_12() { return &___isEOS_12; }
	inline void set_isEOS_12(bool value)
	{
		___isEOS_12 = value;
	}

	inline static int32_t get_offset_of_isRepeatedFrame_13() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___isRepeatedFrame_13)); }
	inline bool get_isRepeatedFrame_13() const { return ___isRepeatedFrame_13; }
	inline bool* get_address_of_isRepeatedFrame_13() { return &___isRepeatedFrame_13; }
	inline void set_isRepeatedFrame_13(bool value)
	{
		___isRepeatedFrame_13 = value;
	}

	inline static int32_t get_offset_of_isKeyFrame_14() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___isKeyFrame_14)); }
	inline bool get_isKeyFrame_14() const { return ___isKeyFrame_14; }
	inline bool* get_address_of_isKeyFrame_14() { return &___isKeyFrame_14; }
	inline void set_isKeyFrame_14(bool value)
	{
		___isKeyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVFFRAMEINFO_T53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E_H
#ifndef SVFOPENINFO_TACB064007773FBC261C9EDBA22DA78C6D1C367F9_H
#define SVFOPENINFO_TACB064007773FBC261C9EDBA22DA78C6D1C367F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVFOpenInfo
struct  SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9 
{
public:
	// System.Boolean SVFOpenInfo::AudioDisabled
	bool ___AudioDisabled_0;
	// System.Boolean SVFOpenInfo::UseHWTexture
	bool ___UseHWTexture_1;
	// System.Boolean SVFOpenInfo::UseHWDecode
	bool ___UseHWDecode_2;
	// System.Boolean SVFOpenInfo::UseKeyedMutex
	bool ___UseKeyedMutex_3;
	// System.Boolean SVFOpenInfo::RenderViaClock
	bool ___RenderViaClock_4;
	// System.Boolean SVFOpenInfo::OutputNormals
	bool ___OutputNormals_5;
	// System.Boolean SVFOpenInfo::StartDownloadOnOpen
	bool ___StartDownloadOnOpen_6;
	// System.Boolean SVFOpenInfo::AutoLooping
	bool ___AutoLooping_7;
	// System.Boolean SVFOpenInfo::lockHWTextures
	bool ___lockHWTextures_8;
	// System.Boolean SVFOpenInfo::forceSoftwareClock
	bool ___forceSoftwareClock_9;
	// System.Single SVFOpenInfo::PlaybackRate
	float ___PlaybackRate_10;
	// System.Single SVFOpenInfo::HRTFMinGain
	float ___HRTFMinGain_11;
	// System.Single SVFOpenInfo::HRTFMaxGain
	float ___HRTFMaxGain_12;
	// System.Single SVFOpenInfo::HRTFGainDistance
	float ___HRTFGainDistance_13;
	// System.Single SVFOpenInfo::HRTFCutoffDistance
	float ___HRTFCutoffDistance_14;
	// System.String SVFOpenInfo::AudioDeviceId
	String_t* ___AudioDeviceId_15;

public:
	inline static int32_t get_offset_of_AudioDisabled_0() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___AudioDisabled_0)); }
	inline bool get_AudioDisabled_0() const { return ___AudioDisabled_0; }
	inline bool* get_address_of_AudioDisabled_0() { return &___AudioDisabled_0; }
	inline void set_AudioDisabled_0(bool value)
	{
		___AudioDisabled_0 = value;
	}

	inline static int32_t get_offset_of_UseHWTexture_1() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___UseHWTexture_1)); }
	inline bool get_UseHWTexture_1() const { return ___UseHWTexture_1; }
	inline bool* get_address_of_UseHWTexture_1() { return &___UseHWTexture_1; }
	inline void set_UseHWTexture_1(bool value)
	{
		___UseHWTexture_1 = value;
	}

	inline static int32_t get_offset_of_UseHWDecode_2() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___UseHWDecode_2)); }
	inline bool get_UseHWDecode_2() const { return ___UseHWDecode_2; }
	inline bool* get_address_of_UseHWDecode_2() { return &___UseHWDecode_2; }
	inline void set_UseHWDecode_2(bool value)
	{
		___UseHWDecode_2 = value;
	}

	inline static int32_t get_offset_of_UseKeyedMutex_3() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___UseKeyedMutex_3)); }
	inline bool get_UseKeyedMutex_3() const { return ___UseKeyedMutex_3; }
	inline bool* get_address_of_UseKeyedMutex_3() { return &___UseKeyedMutex_3; }
	inline void set_UseKeyedMutex_3(bool value)
	{
		___UseKeyedMutex_3 = value;
	}

	inline static int32_t get_offset_of_RenderViaClock_4() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___RenderViaClock_4)); }
	inline bool get_RenderViaClock_4() const { return ___RenderViaClock_4; }
	inline bool* get_address_of_RenderViaClock_4() { return &___RenderViaClock_4; }
	inline void set_RenderViaClock_4(bool value)
	{
		___RenderViaClock_4 = value;
	}

	inline static int32_t get_offset_of_OutputNormals_5() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___OutputNormals_5)); }
	inline bool get_OutputNormals_5() const { return ___OutputNormals_5; }
	inline bool* get_address_of_OutputNormals_5() { return &___OutputNormals_5; }
	inline void set_OutputNormals_5(bool value)
	{
		___OutputNormals_5 = value;
	}

	inline static int32_t get_offset_of_StartDownloadOnOpen_6() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___StartDownloadOnOpen_6)); }
	inline bool get_StartDownloadOnOpen_6() const { return ___StartDownloadOnOpen_6; }
	inline bool* get_address_of_StartDownloadOnOpen_6() { return &___StartDownloadOnOpen_6; }
	inline void set_StartDownloadOnOpen_6(bool value)
	{
		___StartDownloadOnOpen_6 = value;
	}

	inline static int32_t get_offset_of_AutoLooping_7() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___AutoLooping_7)); }
	inline bool get_AutoLooping_7() const { return ___AutoLooping_7; }
	inline bool* get_address_of_AutoLooping_7() { return &___AutoLooping_7; }
	inline void set_AutoLooping_7(bool value)
	{
		___AutoLooping_7 = value;
	}

	inline static int32_t get_offset_of_lockHWTextures_8() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___lockHWTextures_8)); }
	inline bool get_lockHWTextures_8() const { return ___lockHWTextures_8; }
	inline bool* get_address_of_lockHWTextures_8() { return &___lockHWTextures_8; }
	inline void set_lockHWTextures_8(bool value)
	{
		___lockHWTextures_8 = value;
	}

	inline static int32_t get_offset_of_forceSoftwareClock_9() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___forceSoftwareClock_9)); }
	inline bool get_forceSoftwareClock_9() const { return ___forceSoftwareClock_9; }
	inline bool* get_address_of_forceSoftwareClock_9() { return &___forceSoftwareClock_9; }
	inline void set_forceSoftwareClock_9(bool value)
	{
		___forceSoftwareClock_9 = value;
	}

	inline static int32_t get_offset_of_PlaybackRate_10() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___PlaybackRate_10)); }
	inline float get_PlaybackRate_10() const { return ___PlaybackRate_10; }
	inline float* get_address_of_PlaybackRate_10() { return &___PlaybackRate_10; }
	inline void set_PlaybackRate_10(float value)
	{
		___PlaybackRate_10 = value;
	}

	inline static int32_t get_offset_of_HRTFMinGain_11() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___HRTFMinGain_11)); }
	inline float get_HRTFMinGain_11() const { return ___HRTFMinGain_11; }
	inline float* get_address_of_HRTFMinGain_11() { return &___HRTFMinGain_11; }
	inline void set_HRTFMinGain_11(float value)
	{
		___HRTFMinGain_11 = value;
	}

	inline static int32_t get_offset_of_HRTFMaxGain_12() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___HRTFMaxGain_12)); }
	inline float get_HRTFMaxGain_12() const { return ___HRTFMaxGain_12; }
	inline float* get_address_of_HRTFMaxGain_12() { return &___HRTFMaxGain_12; }
	inline void set_HRTFMaxGain_12(float value)
	{
		___HRTFMaxGain_12 = value;
	}

	inline static int32_t get_offset_of_HRTFGainDistance_13() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___HRTFGainDistance_13)); }
	inline float get_HRTFGainDistance_13() const { return ___HRTFGainDistance_13; }
	inline float* get_address_of_HRTFGainDistance_13() { return &___HRTFGainDistance_13; }
	inline void set_HRTFGainDistance_13(float value)
	{
		___HRTFGainDistance_13 = value;
	}

	inline static int32_t get_offset_of_HRTFCutoffDistance_14() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___HRTFCutoffDistance_14)); }
	inline float get_HRTFCutoffDistance_14() const { return ___HRTFCutoffDistance_14; }
	inline float* get_address_of_HRTFCutoffDistance_14() { return &___HRTFCutoffDistance_14; }
	inline void set_HRTFCutoffDistance_14(float value)
	{
		___HRTFCutoffDistance_14 = value;
	}

	inline static int32_t get_offset_of_AudioDeviceId_15() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___AudioDeviceId_15)); }
	inline String_t* get_AudioDeviceId_15() const { return ___AudioDeviceId_15; }
	inline String_t** get_address_of_AudioDeviceId_15() { return &___AudioDeviceId_15; }
	inline void set_AudioDeviceId_15(String_t* value)
	{
		___AudioDeviceId_15 = value;
		Il2CppCodeGenWriteBarrier((&___AudioDeviceId_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of SVFOpenInfo
struct SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9_marshaled_pinvoke
{
	int8_t ___AudioDisabled_0;
	int8_t ___UseHWTexture_1;
	int8_t ___UseHWDecode_2;
	int8_t ___UseKeyedMutex_3;
	int8_t ___RenderViaClock_4;
	int8_t ___OutputNormals_5;
	int8_t ___StartDownloadOnOpen_6;
	int8_t ___AutoLooping_7;
	int8_t ___lockHWTextures_8;
	int8_t ___forceSoftwareClock_9;
	float ___PlaybackRate_10;
	float ___HRTFMinGain_11;
	float ___HRTFMaxGain_12;
	float ___HRTFGainDistance_13;
	float ___HRTFCutoffDistance_14;
	Il2CppChar ___AudioDeviceId_15[1024];
};
// Native definition for COM marshalling of SVFOpenInfo
struct SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9_marshaled_com
{
	int8_t ___AudioDisabled_0;
	int8_t ___UseHWTexture_1;
	int8_t ___UseHWDecode_2;
	int8_t ___UseKeyedMutex_3;
	int8_t ___RenderViaClock_4;
	int8_t ___OutputNormals_5;
	int8_t ___StartDownloadOnOpen_6;
	int8_t ___AutoLooping_7;
	int8_t ___lockHWTextures_8;
	int8_t ___forceSoftwareClock_9;
	float ___PlaybackRate_10;
	float ___HRTFMinGain_11;
	float ___HRTFMaxGain_12;
	float ___HRTFGainDistance_13;
	float ___HRTFCutoffDistance_14;
	Il2CppChar ___AudioDeviceId_15[1024];
};
#endif // SVFOPENINFO_TACB064007773FBC261C9EDBA22DA78C6D1C367F9_H
#ifndef SVFSTATUSINTEROP_TABB537FA071DFAC368BC5DB4720FA1B92B181696_H
#define SVFSTATUSINTEROP_TABB537FA071DFAC368BC5DB4720FA1B92B181696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVFStatusInterop
struct  SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696 
{
public:
	// System.Boolean SVFStatusInterop::isLiveSVFSource
	bool ___isLiveSVFSource_0;
	// System.UInt32 SVFStatusInterop::lastReadFrame
	uint32_t ___lastReadFrame_1;
	// System.UInt32 SVFStatusInterop::unsuccessfulReadFrameCount
	uint32_t ___unsuccessfulReadFrameCount_2;
	// System.UInt32 SVFStatusInterop::droppedFrameCount
	uint32_t ___droppedFrameCount_3;
	// System.UInt32 SVFStatusInterop::errorHresult
	uint32_t ___errorHresult_4;
	// System.Int32 SVFStatusInterop::lastKnownState
	int32_t ___lastKnownState_5;

public:
	inline static int32_t get_offset_of_isLiveSVFSource_0() { return static_cast<int32_t>(offsetof(SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696, ___isLiveSVFSource_0)); }
	inline bool get_isLiveSVFSource_0() const { return ___isLiveSVFSource_0; }
	inline bool* get_address_of_isLiveSVFSource_0() { return &___isLiveSVFSource_0; }
	inline void set_isLiveSVFSource_0(bool value)
	{
		___isLiveSVFSource_0 = value;
	}

	inline static int32_t get_offset_of_lastReadFrame_1() { return static_cast<int32_t>(offsetof(SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696, ___lastReadFrame_1)); }
	inline uint32_t get_lastReadFrame_1() const { return ___lastReadFrame_1; }
	inline uint32_t* get_address_of_lastReadFrame_1() { return &___lastReadFrame_1; }
	inline void set_lastReadFrame_1(uint32_t value)
	{
		___lastReadFrame_1 = value;
	}

	inline static int32_t get_offset_of_unsuccessfulReadFrameCount_2() { return static_cast<int32_t>(offsetof(SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696, ___unsuccessfulReadFrameCount_2)); }
	inline uint32_t get_unsuccessfulReadFrameCount_2() const { return ___unsuccessfulReadFrameCount_2; }
	inline uint32_t* get_address_of_unsuccessfulReadFrameCount_2() { return &___unsuccessfulReadFrameCount_2; }
	inline void set_unsuccessfulReadFrameCount_2(uint32_t value)
	{
		___unsuccessfulReadFrameCount_2 = value;
	}

	inline static int32_t get_offset_of_droppedFrameCount_3() { return static_cast<int32_t>(offsetof(SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696, ___droppedFrameCount_3)); }
	inline uint32_t get_droppedFrameCount_3() const { return ___droppedFrameCount_3; }
	inline uint32_t* get_address_of_droppedFrameCount_3() { return &___droppedFrameCount_3; }
	inline void set_droppedFrameCount_3(uint32_t value)
	{
		___droppedFrameCount_3 = value;
	}

	inline static int32_t get_offset_of_errorHresult_4() { return static_cast<int32_t>(offsetof(SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696, ___errorHresult_4)); }
	inline uint32_t get_errorHresult_4() const { return ___errorHresult_4; }
	inline uint32_t* get_address_of_errorHresult_4() { return &___errorHresult_4; }
	inline void set_errorHresult_4(uint32_t value)
	{
		___errorHresult_4 = value;
	}

	inline static int32_t get_offset_of_lastKnownState_5() { return static_cast<int32_t>(offsetof(SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696, ___lastKnownState_5)); }
	inline int32_t get_lastKnownState_5() const { return ___lastKnownState_5; }
	inline int32_t* get_address_of_lastKnownState_5() { return &___lastKnownState_5; }
	inline void set_lastKnownState_5(int32_t value)
	{
		___lastKnownState_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVFSTATUSINTEROP_TABB537FA071DFAC368BC5DB4720FA1B92B181696_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T489DA527A77CE0DCFEBCD2BA969EEB82D410E998_H
#define NULLABLE_1_T489DA527A77CE0DCFEBCD2BA969EEB82D410E998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T489DA527A77CE0DCFEBCD2BA969EEB82D410E998_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef ARWORLDMAPREQUEST_T4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167_H
#define ARWORLDMAPREQUEST_T4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARWorldMapRequest
struct  ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARWorldMapRequest::m_RequestId
	int32_t ___m_RequestId_0;

public:
	inline static int32_t get_offset_of_m_RequestId_0() { return static_cast<int32_t>(offsetof(ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167, ___m_RequestId_0)); }
	inline int32_t get_m_RequestId_0() const { return ___m_RequestId_0; }
	inline int32_t* get_address_of_m_RequestId_0() { return &___m_RequestId_0; }
	inline void set_m_RequestId_0(int32_t value)
	{
		___m_RequestId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARWORLDMAPREQUEST_T4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167_H
#ifndef VECTOR3INTEROP_T2EF806C1E83787D26A48D437D52765DB172117DA_H
#define VECTOR3INTEROP_T2EF806C1E83787D26A48D437D52765DB172117DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vector3Interop
struct  Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA 
{
public:
	// System.Single Vector3Interop::x
	float ___x_0;
	// System.Single Vector3Interop::y
	float ___y_1;
	// System.Single Vector3Interop::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3INTEROP_T2EF806C1E83787D26A48D437D52765DB172117DA_H
#ifndef U3CSAVEU3ED__27_TAAAF45083EB844EA39B7CB4CF64187F93C5A676B_H
#define U3CSAVEU3ED__27_TAAAF45083EB844EA39B7CB4CF64187F93C5A676B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARWorldMapController_<Save>d__27
struct  U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B  : public RuntimeObject
{
public:
	// System.Int32 ARWorldMapController_<Save>d__27::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ARWorldMapController_<Save>d__27::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ARWorldMapController ARWorldMapController_<Save>d__27::<>4__this
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 * ___U3CU3E4__this_2;
	// UnityEngine.XR.ARKit.ARWorldMapRequest ARWorldMapController_<Save>d__27::<request>5__2
	ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167  ___U3CrequestU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B, ___U3CU3E4__this_2)); }
	inline ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B, ___U3CrequestU3E5__2_3)); }
	inline ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167  get_U3CrequestU3E5__2_3() const { return ___U3CrequestU3E5__2_3; }
	inline ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167 * get_address_of_U3CrequestU3E5__2_3() { return &___U3CrequestU3E5__2_3; }
	inline void set_U3CrequestU3E5__2_3(ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167  value)
	{
		___U3CrequestU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAVEU3ED__27_TAAAF45083EB844EA39B7CB4CF64187F93C5A676B_H
#ifndef LOGLEVEL_T614E5F716939DAB7D93BA30FEC1372E436EC62DE_H
#define LOGLEVEL_T614E5F716939DAB7D93BA30FEC1372E436EC62DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LogLevel
struct  LogLevel_t614E5F716939DAB7D93BA30FEC1372E436EC62DE 
{
public:
	// System.Int32 LogLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogLevel_t614E5F716939DAB7D93BA30FEC1372E436EC62DE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVEL_T614E5F716939DAB7D93BA30FEC1372E436EC62DE_H
#ifndef TOKEN_T53209D188AFD8042884B6D26CC7E3146600F5182_H
#define TOKEN_T53209D188AFD8042884B6D26CC7E3146600F5182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniJSON.Json_Parser_TOKEN
struct  TOKEN_t53209D188AFD8042884B6D26CC7E3146600F5182 
{
public:
	// System.Int32 MiniJSON.Json_Parser_TOKEN::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TOKEN_t53209D188AFD8042884B6D26CC7E3146600F5182, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T53209D188AFD8042884B6D26CC7E3146600F5182_H
#ifndef SVFPLAYBACKSTATE_T0D3DA5DF6EED7D29EE64D6C40C895D3155698B6F_H
#define SVFPLAYBACKSTATE_T0D3DA5DF6EED7D29EE64D6C40C895D3155698B6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVFPlaybackState
struct  SVFPlaybackState_t0D3DA5DF6EED7D29EE64D6C40C895D3155698B6F 
{
public:
	// System.Int32 SVFPlaybackState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SVFPlaybackState_t0D3DA5DF6EED7D29EE64D6C40C895D3155698B6F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVFPLAYBACKSTATE_T0D3DA5DF6EED7D29EE64D6C40C895D3155698B6F_H
#ifndef SVFREADERSTATEINTEROP_T7766360500DB4DD4843D984E227AB74F026ACEA7_H
#define SVFREADERSTATEINTEROP_T7766360500DB4DD4843D984E227AB74F026ACEA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVFReaderStateInterop
struct  SVFReaderStateInterop_t7766360500DB4DD4843D984E227AB74F026ACEA7 
{
public:
	// System.Int32 SVFReaderStateInterop::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SVFReaderStateInterop_t7766360500DB4DD4843D984E227AB74F026ACEA7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVFREADERSTATEINTEROP_T7766360500DB4DD4843D984E227AB74F026ACEA7_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef NULLABLE_1_T9DBDF4088AAEF0A6A6B9D1720769D79A41563E77_H
#define NULLABLE_1_T9DBDF4088AAEF0A6A6B9D1720769D79A41563E77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Color>
struct  Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77 
{
public:
	// T System.Nullable`1::value
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77, ___value_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_value_0() const { return ___value_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9DBDF4088AAEF0A6A6B9D1720769D79A41563E77_H
#ifndef BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#define BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Center_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Extents_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef VRCHANNEL_T5144073EA223E4E52EB5A04144E13FFCB55C9496_H
#define VRCHANNEL_T5144073EA223E4E52EB5A04144E13FFCB55C9496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRChannel
struct  VRChannel_t5144073EA223E4E52EB5A04144E13FFCB55C9496 
{
public:
	// System.Int32 VRChannel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VRChannel_t5144073EA223E4E52EB5A04144E13FFCB55C9496, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRCHANNEL_T5144073EA223E4E52EB5A04144E13FFCB55C9496_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef ONENDOFSTREAM_T6ABE2224F14F21D78185646112D85DE309AA9091_H
#define ONENDOFSTREAM_T6ABE2224F14F21D78185646112D85DE309AA9091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloVideoObject_OnEndOfStream
struct  OnEndOfStream_t6ABE2224F14F21D78185646112D85DE309AA9091  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONENDOFSTREAM_T6ABE2224F14F21D78185646112D85DE309AA9091_H
#ifndef ONFATALERROREVENT_TC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67_H
#define ONFATALERROREVENT_TC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloVideoObject_OnFatalErrorEvent
struct  OnFatalErrorEvent_tC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONFATALERROREVENT_TC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67_H
#ifndef ONFRAMEINFOEVENT_T254A414B4783E99BE1CDD836552B5BEE9BB14C3F_H
#define ONFRAMEINFOEVENT_T254A414B4783E99BE1CDD836552B5BEE9BB14C3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloVideoObject_OnFrameInfoEvent
struct  OnFrameInfoEvent_t254A414B4783E99BE1CDD836552B5BEE9BB14C3F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONFRAMEINFOEVENT_T254A414B4783E99BE1CDD836552B5BEE9BB14C3F_H
#ifndef ONOPENEVENT_T9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1_H
#define ONOPENEVENT_T9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloVideoObject_OnOpenEvent
struct  OnOpenEvent_t9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONOPENEVENT_T9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1_H
#ifndef ONRENDEREVENT_TEBC1B611E970CED1D3F6AE62BCC833366AAC60F6_H
#define ONRENDEREVENT_TEBC1B611E970CED1D3F6AE62BCC833366AAC60F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloVideoObject_OnRenderEvent
struct  OnRenderEvent_tEBC1B611E970CED1D3F6AE62BCC833366AAC60F6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONRENDEREVENT_TEBC1B611E970CED1D3F6AE62BCC833366AAC60F6_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ARFEATHEREDPLANEMESHVISUALIZER_TE778AE37070D3D5969CFD0FEEE8826992D092A67_H
#define ARFEATHEREDPLANEMESHVISUALIZER_TE778AE37070D3D5969CFD0FEEE8826992D092A67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARFeatheredPlaneMeshVisualizer
struct  ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single ARFeatheredPlaneMeshVisualizer::m_FeatheringWidth
	float ___m_FeatheringWidth_4;
	// UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer ARFeatheredPlaneMeshVisualizer::m_PlaneMeshVisualizer
	ARPlaneMeshVisualizer_t1CAC012DACAA7D8D30B94247B8762D71D2624923 * ___m_PlaneMeshVisualizer_7;
	// UnityEngine.Material ARFeatheredPlaneMeshVisualizer::m_FeatheredPlaneMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_FeatheredPlaneMaterial_8;

public:
	inline static int32_t get_offset_of_m_FeatheringWidth_4() { return static_cast<int32_t>(offsetof(ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67, ___m_FeatheringWidth_4)); }
	inline float get_m_FeatheringWidth_4() const { return ___m_FeatheringWidth_4; }
	inline float* get_address_of_m_FeatheringWidth_4() { return &___m_FeatheringWidth_4; }
	inline void set_m_FeatheringWidth_4(float value)
	{
		___m_FeatheringWidth_4 = value;
	}

	inline static int32_t get_offset_of_m_PlaneMeshVisualizer_7() { return static_cast<int32_t>(offsetof(ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67, ___m_PlaneMeshVisualizer_7)); }
	inline ARPlaneMeshVisualizer_t1CAC012DACAA7D8D30B94247B8762D71D2624923 * get_m_PlaneMeshVisualizer_7() const { return ___m_PlaneMeshVisualizer_7; }
	inline ARPlaneMeshVisualizer_t1CAC012DACAA7D8D30B94247B8762D71D2624923 ** get_address_of_m_PlaneMeshVisualizer_7() { return &___m_PlaneMeshVisualizer_7; }
	inline void set_m_PlaneMeshVisualizer_7(ARPlaneMeshVisualizer_t1CAC012DACAA7D8D30B94247B8762D71D2624923 * value)
	{
		___m_PlaneMeshVisualizer_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlaneMeshVisualizer_7), value);
	}

	inline static int32_t get_offset_of_m_FeatheredPlaneMaterial_8() { return static_cast<int32_t>(offsetof(ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67, ___m_FeatheredPlaneMaterial_8)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_FeatheredPlaneMaterial_8() const { return ___m_FeatheredPlaneMaterial_8; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_FeatheredPlaneMaterial_8() { return &___m_FeatheredPlaneMaterial_8; }
	inline void set_m_FeatheredPlaneMaterial_8(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_FeatheredPlaneMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_FeatheredPlaneMaterial_8), value);
	}
};

struct ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> ARFeatheredPlaneMeshVisualizer::s_FeatheringUVs
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___s_FeatheringUVs_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> ARFeatheredPlaneMeshVisualizer::s_Vertices
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___s_Vertices_6;

public:
	inline static int32_t get_offset_of_s_FeatheringUVs_5() { return static_cast<int32_t>(offsetof(ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67_StaticFields, ___s_FeatheringUVs_5)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_s_FeatheringUVs_5() const { return ___s_FeatheringUVs_5; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_s_FeatheringUVs_5() { return &___s_FeatheringUVs_5; }
	inline void set_s_FeatheringUVs_5(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___s_FeatheringUVs_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_FeatheringUVs_5), value);
	}

	inline static int32_t get_offset_of_s_Vertices_6() { return static_cast<int32_t>(offsetof(ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67_StaticFields, ___s_Vertices_6)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_s_Vertices_6() const { return ___s_Vertices_6; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_s_Vertices_6() { return &___s_Vertices_6; }
	inline void set_s_Vertices_6(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___s_Vertices_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Vertices_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARFEATHEREDPLANEMESHVISUALIZER_TE778AE37070D3D5969CFD0FEEE8826992D092A67_H
#ifndef ARWORLDMAPCONTROLLER_TE07FBF1307D659E7AB45F66CFBA14CAC07C98737_H
#define ARWORLDMAPCONTROLLER_TE07FBF1307D659E7AB45F66CFBA14CAC07C98737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARWorldMapController
struct  ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.XR.ARFoundation.ARSession ARWorldMapController::m_ARSession
	ARSession_tFD6F1BD76D4C003B8141D9B6255B904D8C5036AB * ___m_ARSession_4;
	// UnityEngine.UI.Text ARWorldMapController::m_ErrorText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_ErrorText_5;
	// UnityEngine.UI.Text ARWorldMapController::m_LogText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_LogText_6;
	// UnityEngine.UI.Text ARWorldMapController::m_MappingStatusText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_MappingStatusText_7;
	// UnityEngine.UI.Button ARWorldMapController::m_SaveButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_SaveButton_8;
	// UnityEngine.UI.Button ARWorldMapController::m_LoadButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_LoadButton_9;
	// System.Collections.Generic.List`1<System.String> ARWorldMapController::m_LogMessages
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___m_LogMessages_10;

public:
	inline static int32_t get_offset_of_m_ARSession_4() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_ARSession_4)); }
	inline ARSession_tFD6F1BD76D4C003B8141D9B6255B904D8C5036AB * get_m_ARSession_4() const { return ___m_ARSession_4; }
	inline ARSession_tFD6F1BD76D4C003B8141D9B6255B904D8C5036AB ** get_address_of_m_ARSession_4() { return &___m_ARSession_4; }
	inline void set_m_ARSession_4(ARSession_tFD6F1BD76D4C003B8141D9B6255B904D8C5036AB * value)
	{
		___m_ARSession_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ARSession_4), value);
	}

	inline static int32_t get_offset_of_m_ErrorText_5() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_ErrorText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_ErrorText_5() const { return ___m_ErrorText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_ErrorText_5() { return &___m_ErrorText_5; }
	inline void set_m_ErrorText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_ErrorText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ErrorText_5), value);
	}

	inline static int32_t get_offset_of_m_LogText_6() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_LogText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_LogText_6() const { return ___m_LogText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_LogText_6() { return &___m_LogText_6; }
	inline void set_m_LogText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_LogText_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_LogText_6), value);
	}

	inline static int32_t get_offset_of_m_MappingStatusText_7() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_MappingStatusText_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_MappingStatusText_7() const { return ___m_MappingStatusText_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_MappingStatusText_7() { return &___m_MappingStatusText_7; }
	inline void set_m_MappingStatusText_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_MappingStatusText_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_MappingStatusText_7), value);
	}

	inline static int32_t get_offset_of_m_SaveButton_8() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_SaveButton_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_SaveButton_8() const { return ___m_SaveButton_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_SaveButton_8() { return &___m_SaveButton_8; }
	inline void set_m_SaveButton_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_SaveButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_SaveButton_8), value);
	}

	inline static int32_t get_offset_of_m_LoadButton_9() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_LoadButton_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_LoadButton_9() const { return ___m_LoadButton_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_LoadButton_9() { return &___m_LoadButton_9; }
	inline void set_m_LoadButton_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_LoadButton_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_LoadButton_9), value);
	}

	inline static int32_t get_offset_of_m_LogMessages_10() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_LogMessages_10)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_m_LogMessages_10() const { return ___m_LogMessages_10; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_m_LogMessages_10() { return &___m_LogMessages_10; }
	inline void set_m_LogMessages_10(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___m_LogMessages_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_LogMessages_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARWORLDMAPCONTROLLER_TE07FBF1307D659E7AB45F66CFBA14CAC07C98737_H
#ifndef CAMERACONFIGCONTROLLER_T7D9D87FE02FA864F391F3BB2B9E9C7923F85279B_H
#define CAMERACONFIGCONTROLLER_T7D9D87FE02FA864F391F3BB2B9E9C7923F85279B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraConfigController
struct  CameraConfigController_t7D9D87FE02FA864F391F3BB2B9E9C7923F85279B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<System.String> CameraConfigController::m_ConfigurationNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___m_ConfigurationNames_4;
	// UnityEngine.UI.Dropdown CameraConfigController::m_Dropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___m_Dropdown_5;

public:
	inline static int32_t get_offset_of_m_ConfigurationNames_4() { return static_cast<int32_t>(offsetof(CameraConfigController_t7D9D87FE02FA864F391F3BB2B9E9C7923F85279B, ___m_ConfigurationNames_4)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_m_ConfigurationNames_4() const { return ___m_ConfigurationNames_4; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_m_ConfigurationNames_4() { return &___m_ConfigurationNames_4; }
	inline void set_m_ConfigurationNames_4(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___m_ConfigurationNames_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ConfigurationNames_4), value);
	}

	inline static int32_t get_offset_of_m_Dropdown_5() { return static_cast<int32_t>(offsetof(CameraConfigController_t7D9D87FE02FA864F391F3BB2B9E9C7923F85279B, ___m_Dropdown_5)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_m_Dropdown_5() const { return ___m_Dropdown_5; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_m_Dropdown_5() { return &___m_Dropdown_5; }
	inline void set_m_Dropdown_5(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___m_Dropdown_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dropdown_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONFIGCONTROLLER_T7D9D87FE02FA864F391F3BB2B9E9C7923F85279B_H
#ifndef DISABLEVERTICALPLANES_T40082019580280FBDED0DF62CC5DA4E1A8E32D19_H
#define DISABLEVERTICALPLANES_T40082019580280FBDED0DF62CC5DA4E1A8E32D19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisableVerticalPlanes
struct  DisableVerticalPlanes_t40082019580280FBDED0DF62CC5DA4E1A8E32D19  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text DisableVerticalPlanes::m_LogText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_LogText_4;

public:
	inline static int32_t get_offset_of_m_LogText_4() { return static_cast<int32_t>(offsetof(DisableVerticalPlanes_t40082019580280FBDED0DF62CC5DA4E1A8E32D19, ___m_LogText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_LogText_4() const { return ___m_LogText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_LogText_4() { return &___m_LogText_4; }
	inline void set_m_LogText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_LogText_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_LogText_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEVERTICALPLANES_T40082019580280FBDED0DF62CC5DA4E1A8E32D19_H
#ifndef FADEPLANEONBOUNDARYCHANGE_T88629D7DC409498EABE1D1CCE054157CBE47CD7A_H
#define FADEPLANEONBOUNDARYCHANGE_T88629D7DC409498EABE1D1CCE054157CBE47CD7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadePlaneOnBoundaryChange
struct  FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Animator FadePlaneOnBoundaryChange::m_Animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___m_Animator_7;
	// UnityEngine.XR.ARFoundation.ARPlane FadePlaneOnBoundaryChange::m_Plane
	ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E * ___m_Plane_8;
	// System.Single FadePlaneOnBoundaryChange::m_ShowTime
	float ___m_ShowTime_9;
	// System.Boolean FadePlaneOnBoundaryChange::m_UpdatingPlane
	bool ___m_UpdatingPlane_10;

public:
	inline static int32_t get_offset_of_m_Animator_7() { return static_cast<int32_t>(offsetof(FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A, ___m_Animator_7)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_m_Animator_7() const { return ___m_Animator_7; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_m_Animator_7() { return &___m_Animator_7; }
	inline void set_m_Animator_7(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___m_Animator_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Animator_7), value);
	}

	inline static int32_t get_offset_of_m_Plane_8() { return static_cast<int32_t>(offsetof(FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A, ___m_Plane_8)); }
	inline ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E * get_m_Plane_8() const { return ___m_Plane_8; }
	inline ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E ** get_address_of_m_Plane_8() { return &___m_Plane_8; }
	inline void set_m_Plane_8(ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E * value)
	{
		___m_Plane_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Plane_8), value);
	}

	inline static int32_t get_offset_of_m_ShowTime_9() { return static_cast<int32_t>(offsetof(FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A, ___m_ShowTime_9)); }
	inline float get_m_ShowTime_9() const { return ___m_ShowTime_9; }
	inline float* get_address_of_m_ShowTime_9() { return &___m_ShowTime_9; }
	inline void set_m_ShowTime_9(float value)
	{
		___m_ShowTime_9 = value;
	}

	inline static int32_t get_offset_of_m_UpdatingPlane_10() { return static_cast<int32_t>(offsetof(FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A, ___m_UpdatingPlane_10)); }
	inline bool get_m_UpdatingPlane_10() const { return ___m_UpdatingPlane_10; }
	inline bool* get_address_of_m_UpdatingPlane_10() { return &___m_UpdatingPlane_10; }
	inline void set_m_UpdatingPlane_10(bool value)
	{
		___m_UpdatingPlane_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADEPLANEONBOUNDARYCHANGE_T88629D7DC409498EABE1D1CCE054157CBE47CD7A_H
#ifndef HVCONDUCTOR_T966F2F4E311B562E0F5CE66AEEC767CD63E9D59E_H
#define HVCONDUCTOR_T966F2F4E311B562E0F5CE66AEEC767CD63E9D59E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HVConductor
struct  HVConductor_t966F2F4E311B562E0F5CE66AEEC767CD63E9D59E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean HVConductor::ShouldLoopSequences
	bool ___ShouldLoopSequences_4;
	// System.Collections.Generic.List`1<HVConductor_HVSequence> HVConductor::sequenceList
	List_1_t72FCE2C4A1716A632723FDF54D82B4B2F8C3750F * ___sequenceList_5;
	// System.Int32 HVConductor::currSequence
	int32_t ___currSequence_6;

public:
	inline static int32_t get_offset_of_ShouldLoopSequences_4() { return static_cast<int32_t>(offsetof(HVConductor_t966F2F4E311B562E0F5CE66AEEC767CD63E9D59E, ___ShouldLoopSequences_4)); }
	inline bool get_ShouldLoopSequences_4() const { return ___ShouldLoopSequences_4; }
	inline bool* get_address_of_ShouldLoopSequences_4() { return &___ShouldLoopSequences_4; }
	inline void set_ShouldLoopSequences_4(bool value)
	{
		___ShouldLoopSequences_4 = value;
	}

	inline static int32_t get_offset_of_sequenceList_5() { return static_cast<int32_t>(offsetof(HVConductor_t966F2F4E311B562E0F5CE66AEEC767CD63E9D59E, ___sequenceList_5)); }
	inline List_1_t72FCE2C4A1716A632723FDF54D82B4B2F8C3750F * get_sequenceList_5() const { return ___sequenceList_5; }
	inline List_1_t72FCE2C4A1716A632723FDF54D82B4B2F8C3750F ** get_address_of_sequenceList_5() { return &___sequenceList_5; }
	inline void set_sequenceList_5(List_1_t72FCE2C4A1716A632723FDF54D82B4B2F8C3750F * value)
	{
		___sequenceList_5 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceList_5), value);
	}

	inline static int32_t get_offset_of_currSequence_6() { return static_cast<int32_t>(offsetof(HVConductor_t966F2F4E311B562E0F5CE66AEEC767CD63E9D59E, ___currSequence_6)); }
	inline int32_t get_currSequence_6() const { return ___currSequence_6; }
	inline int32_t* get_address_of_currSequence_6() { return &___currSequence_6; }
	inline void set_currSequence_6(int32_t value)
	{
		___currSequence_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HVCONDUCTOR_T966F2F4E311B562E0F5CE66AEEC767CD63E9D59E_H
#ifndef HOLOVIDEOOBJECT_T9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB_H
#define HOLOVIDEOOBJECT_T9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloVideoObject
struct  HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String HoloVideoObject::Url
	String_t* ___Url_4;
	// System.Boolean HoloVideoObject::ShouldAutoPlay
	bool ___ShouldAutoPlay_5;
	// System.Single HoloVideoObject::_audioVolume
	float ____audioVolume_6;
	// UnityEngine.Vector3 HoloVideoObject::audioSourceOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___audioSourceOffset_7;
	// System.Boolean HoloVideoObject::flipHandedness
	bool ___flipHandedness_8;
	// System.Single HoloVideoObject::_clockScale
	float ____clockScale_9;
	// System.Boolean HoloVideoObject::computeNormals
	bool ___computeNormals_10;
	// SVFOpenInfo HoloVideoObject::Settings
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9  ___Settings_11;
	// System.UInt32 HoloVideoObject::DefaultMaxVertexCount
	uint32_t ___DefaultMaxVertexCount_12;
	// System.UInt32 HoloVideoObject::DefaultMaxIndexCount
	uint32_t ___DefaultMaxIndexCount_13;
	// UnityEngine.Bounds HoloVideoObject::localSpaceBounds
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___localSpaceBounds_14;
	// UnityEngine.Mesh[] HoloVideoObject::meshes
	MeshU5BU5D_tDD9C723AA6F0225B35A93D871CDC2CEFF7F8CB89* ___meshes_15;
	// UnityEngine.MeshFilter HoloVideoObject::meshFilter
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * ___meshFilter_16;
	// UnityEngine.MeshRenderer HoloVideoObject::meshRenderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___meshRenderer_17;
	// UnityEngine.BoxCollider HoloVideoObject::HVCollider
	BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * ___HVCollider_18;
	// UnityEngine.Material HoloVideoObject::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_19;
	// UnityEngine.AudioListener HoloVideoObject::listener
	AudioListener_tE3E1467B84A4AFD509947B44A7C8ACFB67FF2099 * ___listener_20;
	// SVFUnityPluginInterop HoloVideoObject::pluginInterop
	SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8 * ___pluginInterop_21;
	// System.Boolean HoloVideoObject::isInitialized
	bool ___isInitialized_22;
	// SVFOpenInfo HoloVideoObject::openInfo
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9  ___openInfo_23;
	// SVFFileInfo HoloVideoObject::fileInfo
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366  ___fileInfo_24;
	// SVFFrameInfo HoloVideoObject::lastFrameInfo
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E  ___lastFrameInfo_25;
	// HoloVideoObject_OnOpenEvent HoloVideoObject::OnOpenNotify
	OnOpenEvent_t9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1 * ___OnOpenNotify_26;
	// HoloVideoObject_OnFrameInfoEvent HoloVideoObject::OnUpdateFrameInfoNotify
	OnFrameInfoEvent_t254A414B4783E99BE1CDD836552B5BEE9BB14C3F * ___OnUpdateFrameInfoNotify_27;
	// HoloVideoObject_OnRenderEvent HoloVideoObject::OnRenderCallback
	OnRenderEvent_tEBC1B611E970CED1D3F6AE62BCC833366AAC60F6 * ___OnRenderCallback_28;
	// HoloVideoObject_OnFatalErrorEvent HoloVideoObject::OnFatalError
	OnFatalErrorEvent_tC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67 * ___OnFatalError_29;
	// HoloVideoObject_OnEndOfStream HoloVideoObject::OnEndOfStreamNotify
	OnEndOfStream_t6ABE2224F14F21D78185646112D85DE309AA9091 * ___OnEndOfStreamNotify_30;
	// UnityEngine.Coroutine HoloVideoObject::UnityBufferCoroutine
	Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * ___UnityBufferCoroutine_31;
	// UnityEngine.Logger HoloVideoObject::logger
	Logger_tEB67FD7450076B84B97F22DBE8B2911FC4FBC35F * ___logger_32;
	// System.Boolean HoloVideoObject::wasPlaying
	bool ___wasPlaying_34;
	// System.Boolean HoloVideoObject::ShouldPauseAfterPlay
	bool ___ShouldPauseAfterPlay_35;
	// System.UInt32 HoloVideoObject::PauseFrameID
	uint32_t ___PauseFrameID_36;
	// System.Boolean HoloVideoObject::isInstanceDisposed
	bool ___isInstanceDisposed_37;

public:
	inline static int32_t get_offset_of_Url_4() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___Url_4)); }
	inline String_t* get_Url_4() const { return ___Url_4; }
	inline String_t** get_address_of_Url_4() { return &___Url_4; }
	inline void set_Url_4(String_t* value)
	{
		___Url_4 = value;
		Il2CppCodeGenWriteBarrier((&___Url_4), value);
	}

	inline static int32_t get_offset_of_ShouldAutoPlay_5() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___ShouldAutoPlay_5)); }
	inline bool get_ShouldAutoPlay_5() const { return ___ShouldAutoPlay_5; }
	inline bool* get_address_of_ShouldAutoPlay_5() { return &___ShouldAutoPlay_5; }
	inline void set_ShouldAutoPlay_5(bool value)
	{
		___ShouldAutoPlay_5 = value;
	}

	inline static int32_t get_offset_of__audioVolume_6() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ____audioVolume_6)); }
	inline float get__audioVolume_6() const { return ____audioVolume_6; }
	inline float* get_address_of__audioVolume_6() { return &____audioVolume_6; }
	inline void set__audioVolume_6(float value)
	{
		____audioVolume_6 = value;
	}

	inline static int32_t get_offset_of_audioSourceOffset_7() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___audioSourceOffset_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_audioSourceOffset_7() const { return ___audioSourceOffset_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_audioSourceOffset_7() { return &___audioSourceOffset_7; }
	inline void set_audioSourceOffset_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___audioSourceOffset_7 = value;
	}

	inline static int32_t get_offset_of_flipHandedness_8() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___flipHandedness_8)); }
	inline bool get_flipHandedness_8() const { return ___flipHandedness_8; }
	inline bool* get_address_of_flipHandedness_8() { return &___flipHandedness_8; }
	inline void set_flipHandedness_8(bool value)
	{
		___flipHandedness_8 = value;
	}

	inline static int32_t get_offset_of__clockScale_9() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ____clockScale_9)); }
	inline float get__clockScale_9() const { return ____clockScale_9; }
	inline float* get_address_of__clockScale_9() { return &____clockScale_9; }
	inline void set__clockScale_9(float value)
	{
		____clockScale_9 = value;
	}

	inline static int32_t get_offset_of_computeNormals_10() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___computeNormals_10)); }
	inline bool get_computeNormals_10() const { return ___computeNormals_10; }
	inline bool* get_address_of_computeNormals_10() { return &___computeNormals_10; }
	inline void set_computeNormals_10(bool value)
	{
		___computeNormals_10 = value;
	}

	inline static int32_t get_offset_of_Settings_11() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___Settings_11)); }
	inline SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9  get_Settings_11() const { return ___Settings_11; }
	inline SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9 * get_address_of_Settings_11() { return &___Settings_11; }
	inline void set_Settings_11(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9  value)
	{
		___Settings_11 = value;
	}

	inline static int32_t get_offset_of_DefaultMaxVertexCount_12() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___DefaultMaxVertexCount_12)); }
	inline uint32_t get_DefaultMaxVertexCount_12() const { return ___DefaultMaxVertexCount_12; }
	inline uint32_t* get_address_of_DefaultMaxVertexCount_12() { return &___DefaultMaxVertexCount_12; }
	inline void set_DefaultMaxVertexCount_12(uint32_t value)
	{
		___DefaultMaxVertexCount_12 = value;
	}

	inline static int32_t get_offset_of_DefaultMaxIndexCount_13() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___DefaultMaxIndexCount_13)); }
	inline uint32_t get_DefaultMaxIndexCount_13() const { return ___DefaultMaxIndexCount_13; }
	inline uint32_t* get_address_of_DefaultMaxIndexCount_13() { return &___DefaultMaxIndexCount_13; }
	inline void set_DefaultMaxIndexCount_13(uint32_t value)
	{
		___DefaultMaxIndexCount_13 = value;
	}

	inline static int32_t get_offset_of_localSpaceBounds_14() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___localSpaceBounds_14)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_localSpaceBounds_14() const { return ___localSpaceBounds_14; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_localSpaceBounds_14() { return &___localSpaceBounds_14; }
	inline void set_localSpaceBounds_14(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___localSpaceBounds_14 = value;
	}

	inline static int32_t get_offset_of_meshes_15() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___meshes_15)); }
	inline MeshU5BU5D_tDD9C723AA6F0225B35A93D871CDC2CEFF7F8CB89* get_meshes_15() const { return ___meshes_15; }
	inline MeshU5BU5D_tDD9C723AA6F0225B35A93D871CDC2CEFF7F8CB89** get_address_of_meshes_15() { return &___meshes_15; }
	inline void set_meshes_15(MeshU5BU5D_tDD9C723AA6F0225B35A93D871CDC2CEFF7F8CB89* value)
	{
		___meshes_15 = value;
		Il2CppCodeGenWriteBarrier((&___meshes_15), value);
	}

	inline static int32_t get_offset_of_meshFilter_16() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___meshFilter_16)); }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * get_meshFilter_16() const { return ___meshFilter_16; }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 ** get_address_of_meshFilter_16() { return &___meshFilter_16; }
	inline void set_meshFilter_16(MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * value)
	{
		___meshFilter_16 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_16), value);
	}

	inline static int32_t get_offset_of_meshRenderer_17() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___meshRenderer_17)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_meshRenderer_17() const { return ___meshRenderer_17; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_meshRenderer_17() { return &___meshRenderer_17; }
	inline void set_meshRenderer_17(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___meshRenderer_17 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_17), value);
	}

	inline static int32_t get_offset_of_HVCollider_18() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___HVCollider_18)); }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * get_HVCollider_18() const { return ___HVCollider_18; }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA ** get_address_of_HVCollider_18() { return &___HVCollider_18; }
	inline void set_HVCollider_18(BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * value)
	{
		___HVCollider_18 = value;
		Il2CppCodeGenWriteBarrier((&___HVCollider_18), value);
	}

	inline static int32_t get_offset_of_material_19() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___material_19)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_19() const { return ___material_19; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_19() { return &___material_19; }
	inline void set_material_19(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_19 = value;
		Il2CppCodeGenWriteBarrier((&___material_19), value);
	}

	inline static int32_t get_offset_of_listener_20() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___listener_20)); }
	inline AudioListener_tE3E1467B84A4AFD509947B44A7C8ACFB67FF2099 * get_listener_20() const { return ___listener_20; }
	inline AudioListener_tE3E1467B84A4AFD509947B44A7C8ACFB67FF2099 ** get_address_of_listener_20() { return &___listener_20; }
	inline void set_listener_20(AudioListener_tE3E1467B84A4AFD509947B44A7C8ACFB67FF2099 * value)
	{
		___listener_20 = value;
		Il2CppCodeGenWriteBarrier((&___listener_20), value);
	}

	inline static int32_t get_offset_of_pluginInterop_21() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___pluginInterop_21)); }
	inline SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8 * get_pluginInterop_21() const { return ___pluginInterop_21; }
	inline SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8 ** get_address_of_pluginInterop_21() { return &___pluginInterop_21; }
	inline void set_pluginInterop_21(SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8 * value)
	{
		___pluginInterop_21 = value;
		Il2CppCodeGenWriteBarrier((&___pluginInterop_21), value);
	}

	inline static int32_t get_offset_of_isInitialized_22() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___isInitialized_22)); }
	inline bool get_isInitialized_22() const { return ___isInitialized_22; }
	inline bool* get_address_of_isInitialized_22() { return &___isInitialized_22; }
	inline void set_isInitialized_22(bool value)
	{
		___isInitialized_22 = value;
	}

	inline static int32_t get_offset_of_openInfo_23() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___openInfo_23)); }
	inline SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9  get_openInfo_23() const { return ___openInfo_23; }
	inline SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9 * get_address_of_openInfo_23() { return &___openInfo_23; }
	inline void set_openInfo_23(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9  value)
	{
		___openInfo_23 = value;
	}

	inline static int32_t get_offset_of_fileInfo_24() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___fileInfo_24)); }
	inline SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366  get_fileInfo_24() const { return ___fileInfo_24; }
	inline SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366 * get_address_of_fileInfo_24() { return &___fileInfo_24; }
	inline void set_fileInfo_24(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366  value)
	{
		___fileInfo_24 = value;
	}

	inline static int32_t get_offset_of_lastFrameInfo_25() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___lastFrameInfo_25)); }
	inline SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E  get_lastFrameInfo_25() const { return ___lastFrameInfo_25; }
	inline SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E * get_address_of_lastFrameInfo_25() { return &___lastFrameInfo_25; }
	inline void set_lastFrameInfo_25(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E  value)
	{
		___lastFrameInfo_25 = value;
	}

	inline static int32_t get_offset_of_OnOpenNotify_26() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___OnOpenNotify_26)); }
	inline OnOpenEvent_t9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1 * get_OnOpenNotify_26() const { return ___OnOpenNotify_26; }
	inline OnOpenEvent_t9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1 ** get_address_of_OnOpenNotify_26() { return &___OnOpenNotify_26; }
	inline void set_OnOpenNotify_26(OnOpenEvent_t9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1 * value)
	{
		___OnOpenNotify_26 = value;
		Il2CppCodeGenWriteBarrier((&___OnOpenNotify_26), value);
	}

	inline static int32_t get_offset_of_OnUpdateFrameInfoNotify_27() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___OnUpdateFrameInfoNotify_27)); }
	inline OnFrameInfoEvent_t254A414B4783E99BE1CDD836552B5BEE9BB14C3F * get_OnUpdateFrameInfoNotify_27() const { return ___OnUpdateFrameInfoNotify_27; }
	inline OnFrameInfoEvent_t254A414B4783E99BE1CDD836552B5BEE9BB14C3F ** get_address_of_OnUpdateFrameInfoNotify_27() { return &___OnUpdateFrameInfoNotify_27; }
	inline void set_OnUpdateFrameInfoNotify_27(OnFrameInfoEvent_t254A414B4783E99BE1CDD836552B5BEE9BB14C3F * value)
	{
		___OnUpdateFrameInfoNotify_27 = value;
		Il2CppCodeGenWriteBarrier((&___OnUpdateFrameInfoNotify_27), value);
	}

	inline static int32_t get_offset_of_OnRenderCallback_28() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___OnRenderCallback_28)); }
	inline OnRenderEvent_tEBC1B611E970CED1D3F6AE62BCC833366AAC60F6 * get_OnRenderCallback_28() const { return ___OnRenderCallback_28; }
	inline OnRenderEvent_tEBC1B611E970CED1D3F6AE62BCC833366AAC60F6 ** get_address_of_OnRenderCallback_28() { return &___OnRenderCallback_28; }
	inline void set_OnRenderCallback_28(OnRenderEvent_tEBC1B611E970CED1D3F6AE62BCC833366AAC60F6 * value)
	{
		___OnRenderCallback_28 = value;
		Il2CppCodeGenWriteBarrier((&___OnRenderCallback_28), value);
	}

	inline static int32_t get_offset_of_OnFatalError_29() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___OnFatalError_29)); }
	inline OnFatalErrorEvent_tC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67 * get_OnFatalError_29() const { return ___OnFatalError_29; }
	inline OnFatalErrorEvent_tC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67 ** get_address_of_OnFatalError_29() { return &___OnFatalError_29; }
	inline void set_OnFatalError_29(OnFatalErrorEvent_tC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67 * value)
	{
		___OnFatalError_29 = value;
		Il2CppCodeGenWriteBarrier((&___OnFatalError_29), value);
	}

	inline static int32_t get_offset_of_OnEndOfStreamNotify_30() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___OnEndOfStreamNotify_30)); }
	inline OnEndOfStream_t6ABE2224F14F21D78185646112D85DE309AA9091 * get_OnEndOfStreamNotify_30() const { return ___OnEndOfStreamNotify_30; }
	inline OnEndOfStream_t6ABE2224F14F21D78185646112D85DE309AA9091 ** get_address_of_OnEndOfStreamNotify_30() { return &___OnEndOfStreamNotify_30; }
	inline void set_OnEndOfStreamNotify_30(OnEndOfStream_t6ABE2224F14F21D78185646112D85DE309AA9091 * value)
	{
		___OnEndOfStreamNotify_30 = value;
		Il2CppCodeGenWriteBarrier((&___OnEndOfStreamNotify_30), value);
	}

	inline static int32_t get_offset_of_UnityBufferCoroutine_31() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___UnityBufferCoroutine_31)); }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * get_UnityBufferCoroutine_31() const { return ___UnityBufferCoroutine_31; }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC ** get_address_of_UnityBufferCoroutine_31() { return &___UnityBufferCoroutine_31; }
	inline void set_UnityBufferCoroutine_31(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * value)
	{
		___UnityBufferCoroutine_31 = value;
		Il2CppCodeGenWriteBarrier((&___UnityBufferCoroutine_31), value);
	}

	inline static int32_t get_offset_of_logger_32() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___logger_32)); }
	inline Logger_tEB67FD7450076B84B97F22DBE8B2911FC4FBC35F * get_logger_32() const { return ___logger_32; }
	inline Logger_tEB67FD7450076B84B97F22DBE8B2911FC4FBC35F ** get_address_of_logger_32() { return &___logger_32; }
	inline void set_logger_32(Logger_tEB67FD7450076B84B97F22DBE8B2911FC4FBC35F * value)
	{
		___logger_32 = value;
		Il2CppCodeGenWriteBarrier((&___logger_32), value);
	}

	inline static int32_t get_offset_of_wasPlaying_34() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___wasPlaying_34)); }
	inline bool get_wasPlaying_34() const { return ___wasPlaying_34; }
	inline bool* get_address_of_wasPlaying_34() { return &___wasPlaying_34; }
	inline void set_wasPlaying_34(bool value)
	{
		___wasPlaying_34 = value;
	}

	inline static int32_t get_offset_of_ShouldPauseAfterPlay_35() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___ShouldPauseAfterPlay_35)); }
	inline bool get_ShouldPauseAfterPlay_35() const { return ___ShouldPauseAfterPlay_35; }
	inline bool* get_address_of_ShouldPauseAfterPlay_35() { return &___ShouldPauseAfterPlay_35; }
	inline void set_ShouldPauseAfterPlay_35(bool value)
	{
		___ShouldPauseAfterPlay_35 = value;
	}

	inline static int32_t get_offset_of_PauseFrameID_36() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___PauseFrameID_36)); }
	inline uint32_t get_PauseFrameID_36() const { return ___PauseFrameID_36; }
	inline uint32_t* get_address_of_PauseFrameID_36() { return &___PauseFrameID_36; }
	inline void set_PauseFrameID_36(uint32_t value)
	{
		___PauseFrameID_36 = value;
	}

	inline static int32_t get_offset_of_isInstanceDisposed_37() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___isInstanceDisposed_37)); }
	inline bool get_isInstanceDisposed_37() const { return ___isInstanceDisposed_37; }
	inline bool* get_address_of_isInstanceDisposed_37() { return &___isInstanceDisposed_37; }
	inline void set_isInstanceDisposed_37(bool value)
	{
		___isInstanceDisposed_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLOVIDEOOBJECT_T9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB_H
#ifndef LIGHTESTIMATION_TAC157B297F822653381F8F35A6DE3600920A4319_H
#define LIGHTESTIMATION_TAC157B297F822653381F8F35A6DE3600920A4319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightEstimation
struct  LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Nullable`1<System.Single> LightEstimation::<brightness>k__BackingField
	Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  ___U3CbrightnessU3Ek__BackingField_4;
	// System.Nullable`1<System.Single> LightEstimation::<colorTemperature>k__BackingField
	Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  ___U3CcolorTemperatureU3Ek__BackingField_5;
	// System.Nullable`1<UnityEngine.Color> LightEstimation::<colorCorrection>k__BackingField
	Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77  ___U3CcolorCorrectionU3Ek__BackingField_6;
	// UnityEngine.Light LightEstimation::m_Light
	Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * ___m_Light_7;

public:
	inline static int32_t get_offset_of_U3CbrightnessU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319, ___U3CbrightnessU3Ek__BackingField_4)); }
	inline Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  get_U3CbrightnessU3Ek__BackingField_4() const { return ___U3CbrightnessU3Ek__BackingField_4; }
	inline Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998 * get_address_of_U3CbrightnessU3Ek__BackingField_4() { return &___U3CbrightnessU3Ek__BackingField_4; }
	inline void set_U3CbrightnessU3Ek__BackingField_4(Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  value)
	{
		___U3CbrightnessU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CcolorTemperatureU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319, ___U3CcolorTemperatureU3Ek__BackingField_5)); }
	inline Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  get_U3CcolorTemperatureU3Ek__BackingField_5() const { return ___U3CcolorTemperatureU3Ek__BackingField_5; }
	inline Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998 * get_address_of_U3CcolorTemperatureU3Ek__BackingField_5() { return &___U3CcolorTemperatureU3Ek__BackingField_5; }
	inline void set_U3CcolorTemperatureU3Ek__BackingField_5(Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  value)
	{
		___U3CcolorTemperatureU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CcolorCorrectionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319, ___U3CcolorCorrectionU3Ek__BackingField_6)); }
	inline Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77  get_U3CcolorCorrectionU3Ek__BackingField_6() const { return ___U3CcolorCorrectionU3Ek__BackingField_6; }
	inline Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77 * get_address_of_U3CcolorCorrectionU3Ek__BackingField_6() { return &___U3CcolorCorrectionU3Ek__BackingField_6; }
	inline void set_U3CcolorCorrectionU3Ek__BackingField_6(Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77  value)
	{
		___U3CcolorCorrectionU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_m_Light_7() { return static_cast<int32_t>(offsetof(LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319, ___m_Light_7)); }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * get_m_Light_7() const { return ___m_Light_7; }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C ** get_address_of_m_Light_7() { return &___m_Light_7; }
	inline void set_m_Light_7(Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * value)
	{
		___m_Light_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Light_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTESTIMATION_TAC157B297F822653381F8F35A6DE3600920A4319_H
#ifndef LIGHTESTIMATIONUI_TABE99F2D6E580AB4AD14174CE323C527882D6DA4_H
#define LIGHTESTIMATIONUI_TABE99F2D6E580AB4AD14174CE323C527882D6DA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightEstimationUI
struct  LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text LightEstimationUI::m_BrightnessText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_BrightnessText_4;
	// UnityEngine.UI.Text LightEstimationUI::m_ColorTemperatureText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_ColorTemperatureText_5;
	// UnityEngine.UI.Text LightEstimationUI::m_ColorCorrectionText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_ColorCorrectionText_6;
	// LightEstimation LightEstimationUI::m_LightEstimation
	LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319 * ___m_LightEstimation_8;

public:
	inline static int32_t get_offset_of_m_BrightnessText_4() { return static_cast<int32_t>(offsetof(LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4, ___m_BrightnessText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_BrightnessText_4() const { return ___m_BrightnessText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_BrightnessText_4() { return &___m_BrightnessText_4; }
	inline void set_m_BrightnessText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_BrightnessText_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_BrightnessText_4), value);
	}

	inline static int32_t get_offset_of_m_ColorTemperatureText_5() { return static_cast<int32_t>(offsetof(LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4, ___m_ColorTemperatureText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_ColorTemperatureText_5() const { return ___m_ColorTemperatureText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_ColorTemperatureText_5() { return &___m_ColorTemperatureText_5; }
	inline void set_m_ColorTemperatureText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_ColorTemperatureText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTemperatureText_5), value);
	}

	inline static int32_t get_offset_of_m_ColorCorrectionText_6() { return static_cast<int32_t>(offsetof(LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4, ___m_ColorCorrectionText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_ColorCorrectionText_6() const { return ___m_ColorCorrectionText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_ColorCorrectionText_6() { return &___m_ColorCorrectionText_6; }
	inline void set_m_ColorCorrectionText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_ColorCorrectionText_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorCorrectionText_6), value);
	}

	inline static int32_t get_offset_of_m_LightEstimation_8() { return static_cast<int32_t>(offsetof(LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4, ___m_LightEstimation_8)); }
	inline LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319 * get_m_LightEstimation_8() const { return ___m_LightEstimation_8; }
	inline LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319 ** get_address_of_m_LightEstimation_8() { return &___m_LightEstimation_8; }
	inline void set_m_LightEstimation_8(LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319 * value)
	{
		___m_LightEstimation_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_LightEstimation_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTESTIMATIONUI_TABE99F2D6E580AB4AD14174CE323C527882D6DA4_H
#ifndef MAKEAPPEARONPLANE_TEFCA4242B61BEEA04B85419F892D7506BF9E7EB3_H
#define MAKEAPPEARONPLANE_TEFCA4242B61BEEA04B85419F892D7506BF9E7EB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MakeAppearOnPlane
struct  MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform MakeAppearOnPlane::m_Content
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_Content_4;
	// UnityEngine.Quaternion MakeAppearOnPlane::m_Rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_Rotation_5;
	// UnityEngine.XR.ARFoundation.ARSessionOrigin MakeAppearOnPlane::m_SessionOrigin
	ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * ___m_SessionOrigin_7;

public:
	inline static int32_t get_offset_of_m_Content_4() { return static_cast<int32_t>(offsetof(MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3, ___m_Content_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_Content_4() const { return ___m_Content_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_Content_4() { return &___m_Content_4; }
	inline void set_m_Content_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_Content_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_4), value);
	}

	inline static int32_t get_offset_of_m_Rotation_5() { return static_cast<int32_t>(offsetof(MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3, ___m_Rotation_5)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_Rotation_5() const { return ___m_Rotation_5; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_Rotation_5() { return &___m_Rotation_5; }
	inline void set_m_Rotation_5(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_Rotation_5 = value;
	}

	inline static int32_t get_offset_of_m_SessionOrigin_7() { return static_cast<int32_t>(offsetof(MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3, ___m_SessionOrigin_7)); }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * get_m_SessionOrigin_7() const { return ___m_SessionOrigin_7; }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF ** get_address_of_m_SessionOrigin_7() { return &___m_SessionOrigin_7; }
	inline void set_m_SessionOrigin_7(ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * value)
	{
		___m_SessionOrigin_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_SessionOrigin_7), value);
	}
};

struct MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit> MakeAppearOnPlane::s_Hits
	List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * ___s_Hits_6;

public:
	inline static int32_t get_offset_of_s_Hits_6() { return static_cast<int32_t>(offsetof(MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3_StaticFields, ___s_Hits_6)); }
	inline List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * get_s_Hits_6() const { return ___s_Hits_6; }
	inline List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 ** get_address_of_s_Hits_6() { return &___s_Hits_6; }
	inline void set_s_Hits_6(List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * value)
	{
		___s_Hits_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Hits_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAKEAPPEARONPLANE_TEFCA4242B61BEEA04B85419F892D7506BF9E7EB3_H
#ifndef PLACEMULTIPLEOBJECTSONPLANE_T776325CC089E260D234CC3B495856446CEB362CC_H
#define PLACEMULTIPLEOBJECTSONPLANE_T776325CC089E260D234CC3B495856446CEB362CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaceMultipleObjectsOnPlane
struct  PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject PlaceMultipleObjectsOnPlane::m_PlacedPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_PlacedPrefab_4;
	// UnityEngine.GameObject PlaceMultipleObjectsOnPlane::<spawnedObject>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CspawnedObjectU3Ek__BackingField_5;
	// UnityEngine.XR.ARFoundation.ARSessionOrigin PlaceMultipleObjectsOnPlane::m_SessionOrigin
	ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * ___m_SessionOrigin_7;

public:
	inline static int32_t get_offset_of_m_PlacedPrefab_4() { return static_cast<int32_t>(offsetof(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC, ___m_PlacedPrefab_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_PlacedPrefab_4() const { return ___m_PlacedPrefab_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_PlacedPrefab_4() { return &___m_PlacedPrefab_4; }
	inline void set_m_PlacedPrefab_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_PlacedPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlacedPrefab_4), value);
	}

	inline static int32_t get_offset_of_U3CspawnedObjectU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC, ___U3CspawnedObjectU3Ek__BackingField_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CspawnedObjectU3Ek__BackingField_5() const { return ___U3CspawnedObjectU3Ek__BackingField_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CspawnedObjectU3Ek__BackingField_5() { return &___U3CspawnedObjectU3Ek__BackingField_5; }
	inline void set_U3CspawnedObjectU3Ek__BackingField_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CspawnedObjectU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CspawnedObjectU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_m_SessionOrigin_7() { return static_cast<int32_t>(offsetof(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC, ___m_SessionOrigin_7)); }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * get_m_SessionOrigin_7() const { return ___m_SessionOrigin_7; }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF ** get_address_of_m_SessionOrigin_7() { return &___m_SessionOrigin_7; }
	inline void set_m_SessionOrigin_7(ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * value)
	{
		___m_SessionOrigin_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_SessionOrigin_7), value);
	}
};

struct PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_StaticFields
{
public:
	// System.Action PlaceMultipleObjectsOnPlane::onPlacedObject
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onPlacedObject_6;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit> PlaceMultipleObjectsOnPlane::s_Hits
	List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * ___s_Hits_8;

public:
	inline static int32_t get_offset_of_onPlacedObject_6() { return static_cast<int32_t>(offsetof(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_StaticFields, ___onPlacedObject_6)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onPlacedObject_6() const { return ___onPlacedObject_6; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onPlacedObject_6() { return &___onPlacedObject_6; }
	inline void set_onPlacedObject_6(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onPlacedObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPlacedObject_6), value);
	}

	inline static int32_t get_offset_of_s_Hits_8() { return static_cast<int32_t>(offsetof(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_StaticFields, ___s_Hits_8)); }
	inline List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * get_s_Hits_8() const { return ___s_Hits_8; }
	inline List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 ** get_address_of_s_Hits_8() { return &___s_Hits_8; }
	inline void set_s_Hits_8(List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * value)
	{
		___s_Hits_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_Hits_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEMULTIPLEOBJECTSONPLANE_T776325CC089E260D234CC3B495856446CEB362CC_H
#ifndef PLACEONPLANE_TC5323EAA9F7603ADAB59DB462174996553CAF0C2_H
#define PLACEONPLANE_TC5323EAA9F7603ADAB59DB462174996553CAF0C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaceOnPlane
struct  PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject PlaceOnPlane::m_PlacedPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_PlacedPrefab_4;
	// UnityEngine.GameObject PlaceOnPlane::<spawnedObject>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CspawnedObjectU3Ek__BackingField_5;
	// UnityEngine.XR.ARFoundation.ARSessionOrigin PlaceOnPlane::m_SessionOrigin
	ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * ___m_SessionOrigin_7;

public:
	inline static int32_t get_offset_of_m_PlacedPrefab_4() { return static_cast<int32_t>(offsetof(PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2, ___m_PlacedPrefab_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_PlacedPrefab_4() const { return ___m_PlacedPrefab_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_PlacedPrefab_4() { return &___m_PlacedPrefab_4; }
	inline void set_m_PlacedPrefab_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_PlacedPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlacedPrefab_4), value);
	}

	inline static int32_t get_offset_of_U3CspawnedObjectU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2, ___U3CspawnedObjectU3Ek__BackingField_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CspawnedObjectU3Ek__BackingField_5() const { return ___U3CspawnedObjectU3Ek__BackingField_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CspawnedObjectU3Ek__BackingField_5() { return &___U3CspawnedObjectU3Ek__BackingField_5; }
	inline void set_U3CspawnedObjectU3Ek__BackingField_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CspawnedObjectU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CspawnedObjectU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_m_SessionOrigin_7() { return static_cast<int32_t>(offsetof(PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2, ___m_SessionOrigin_7)); }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * get_m_SessionOrigin_7() const { return ___m_SessionOrigin_7; }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF ** get_address_of_m_SessionOrigin_7() { return &___m_SessionOrigin_7; }
	inline void set_m_SessionOrigin_7(ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * value)
	{
		___m_SessionOrigin_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_SessionOrigin_7), value);
	}
};

struct PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit> PlaceOnPlane::s_Hits
	List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * ___s_Hits_6;

public:
	inline static int32_t get_offset_of_s_Hits_6() { return static_cast<int32_t>(offsetof(PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2_StaticFields, ___s_Hits_6)); }
	inline List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * get_s_Hits_6() const { return ___s_Hits_6; }
	inline List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 ** get_address_of_s_Hits_6() { return &___s_Hits_6; }
	inline void set_s_Hits_6(List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * value)
	{
		___s_Hits_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Hits_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEONPLANE_TC5323EAA9F7603ADAB59DB462174996553CAF0C2_H
#ifndef PLANEDETECTIONCONTROLLER_TF91A38E8CF0739882A647588DB89B4E0B91482DD_H
#define PLANEDETECTIONCONTROLLER_TF91A38E8CF0739882A647588DB89B4E0B91482DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaneDetectionController
struct  PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text PlaneDetectionController::m_TogglePlaneDetectionText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_TogglePlaneDetectionText_4;
	// UnityEngine.XR.ARFoundation.ARPlaneManager PlaneDetectionController::m_ARPlaneManager
	ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * ___m_ARPlaneManager_5;

public:
	inline static int32_t get_offset_of_m_TogglePlaneDetectionText_4() { return static_cast<int32_t>(offsetof(PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD, ___m_TogglePlaneDetectionText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_TogglePlaneDetectionText_4() const { return ___m_TogglePlaneDetectionText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_TogglePlaneDetectionText_4() { return &___m_TogglePlaneDetectionText_4; }
	inline void set_m_TogglePlaneDetectionText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_TogglePlaneDetectionText_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TogglePlaneDetectionText_4), value);
	}

	inline static int32_t get_offset_of_m_ARPlaneManager_5() { return static_cast<int32_t>(offsetof(PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD, ___m_ARPlaneManager_5)); }
	inline ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * get_m_ARPlaneManager_5() const { return ___m_ARPlaneManager_5; }
	inline ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A ** get_address_of_m_ARPlaneManager_5() { return &___m_ARPlaneManager_5; }
	inline void set_m_ARPlaneManager_5(ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * value)
	{
		___m_ARPlaneManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ARPlaneManager_5), value);
	}
};

struct PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane> PlaneDetectionController::s_Planes
	List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * ___s_Planes_6;

public:
	inline static int32_t get_offset_of_s_Planes_6() { return static_cast<int32_t>(offsetof(PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD_StaticFields, ___s_Planes_6)); }
	inline List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * get_s_Planes_6() const { return ___s_Planes_6; }
	inline List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B ** get_address_of_s_Planes_6() { return &___s_Planes_6; }
	inline void set_s_Planes_6(List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * value)
	{
		___s_Planes_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Planes_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANEDETECTIONCONTROLLER_TF91A38E8CF0739882A647588DB89B4E0B91482DD_H
#ifndef ROTATIONCONTROLLER_T41CB9629E51ECA40E7A0DC705A60605DE218C4EB_H
#define ROTATIONCONTROLLER_T41CB9629E51ECA40E7A0DC705A60605DE218C4EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotationController
struct  RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MakeAppearOnPlane RotationController::m_MakeAppearOnPlane
	MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3 * ___m_MakeAppearOnPlane_4;
	// UnityEngine.UI.Slider RotationController::m_Slider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___m_Slider_5;
	// UnityEngine.UI.Text RotationController::m_Text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_Text_6;
	// System.Single RotationController::m_Min
	float ___m_Min_7;
	// System.Single RotationController::m_Max
	float ___m_Max_8;

public:
	inline static int32_t get_offset_of_m_MakeAppearOnPlane_4() { return static_cast<int32_t>(offsetof(RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB, ___m_MakeAppearOnPlane_4)); }
	inline MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3 * get_m_MakeAppearOnPlane_4() const { return ___m_MakeAppearOnPlane_4; }
	inline MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3 ** get_address_of_m_MakeAppearOnPlane_4() { return &___m_MakeAppearOnPlane_4; }
	inline void set_m_MakeAppearOnPlane_4(MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3 * value)
	{
		___m_MakeAppearOnPlane_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MakeAppearOnPlane_4), value);
	}

	inline static int32_t get_offset_of_m_Slider_5() { return static_cast<int32_t>(offsetof(RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB, ___m_Slider_5)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_m_Slider_5() const { return ___m_Slider_5; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_m_Slider_5() { return &___m_Slider_5; }
	inline void set_m_Slider_5(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___m_Slider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Slider_5), value);
	}

	inline static int32_t get_offset_of_m_Text_6() { return static_cast<int32_t>(offsetof(RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB, ___m_Text_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_Text_6() const { return ___m_Text_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_Text_6() { return &___m_Text_6; }
	inline void set_m_Text_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_Text_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_6), value);
	}

	inline static int32_t get_offset_of_m_Min_7() { return static_cast<int32_t>(offsetof(RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB, ___m_Min_7)); }
	inline float get_m_Min_7() const { return ___m_Min_7; }
	inline float* get_address_of_m_Min_7() { return &___m_Min_7; }
	inline void set_m_Min_7(float value)
	{
		___m_Min_7 = value;
	}

	inline static int32_t get_offset_of_m_Max_8() { return static_cast<int32_t>(offsetof(RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB, ___m_Max_8)); }
	inline float get_m_Max_8() const { return ___m_Max_8; }
	inline float* get_address_of_m_Max_8() { return &___m_Max_8; }
	inline void set_m_Max_8(float value)
	{
		___m_Max_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONCONTROLLER_T41CB9629E51ECA40E7A0DC705A60605DE218C4EB_H
#ifndef SCALECONTROLLER_T4CADE3F801EC93C79203B3407E4086AA4BC3FE1A_H
#define SCALECONTROLLER_T4CADE3F801EC93C79203B3407E4086AA4BC3FE1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScaleController
struct  ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Slider ScaleController::m_Slider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___m_Slider_4;
	// UnityEngine.UI.Text ScaleController::m_Text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_Text_5;
	// System.Single ScaleController::m_Min
	float ___m_Min_6;
	// System.Single ScaleController::m_Max
	float ___m_Max_7;
	// UnityEngine.XR.ARFoundation.ARSessionOrigin ScaleController::m_SessionOrigin
	ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * ___m_SessionOrigin_8;

public:
	inline static int32_t get_offset_of_m_Slider_4() { return static_cast<int32_t>(offsetof(ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A, ___m_Slider_4)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_m_Slider_4() const { return ___m_Slider_4; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_m_Slider_4() { return &___m_Slider_4; }
	inline void set_m_Slider_4(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___m_Slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Slider_4), value);
	}

	inline static int32_t get_offset_of_m_Text_5() { return static_cast<int32_t>(offsetof(ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A, ___m_Text_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_Text_5() const { return ___m_Text_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_Text_5() { return &___m_Text_5; }
	inline void set_m_Text_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_Text_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_5), value);
	}

	inline static int32_t get_offset_of_m_Min_6() { return static_cast<int32_t>(offsetof(ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A, ___m_Min_6)); }
	inline float get_m_Min_6() const { return ___m_Min_6; }
	inline float* get_address_of_m_Min_6() { return &___m_Min_6; }
	inline void set_m_Min_6(float value)
	{
		___m_Min_6 = value;
	}

	inline static int32_t get_offset_of_m_Max_7() { return static_cast<int32_t>(offsetof(ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A, ___m_Max_7)); }
	inline float get_m_Max_7() const { return ___m_Max_7; }
	inline float* get_address_of_m_Max_7() { return &___m_Max_7; }
	inline void set_m_Max_7(float value)
	{
		___m_Max_7 = value;
	}

	inline static int32_t get_offset_of_m_SessionOrigin_8() { return static_cast<int32_t>(offsetof(ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A, ___m_SessionOrigin_8)); }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * get_m_SessionOrigin_8() const { return ___m_SessionOrigin_8; }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF ** get_address_of_m_SessionOrigin_8() { return &___m_SessionOrigin_8; }
	inline void set_m_SessionOrigin_8(ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * value)
	{
		___m_SessionOrigin_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_SessionOrigin_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALECONTROLLER_T4CADE3F801EC93C79203B3407E4086AA4BC3FE1A_H
#ifndef SCREENCAPTURE_T2374541D6035304D83868BE2B54D3C747DF22963_H
#define SCREENCAPTURE_T2374541D6035304D83868BE2B54D3C747DF22963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenCapture
struct  ScreenCapture_t2374541D6035304D83868BE2B54D3C747DF22963  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENCAPTURE_T2374541D6035304D83868BE2B54D3C747DF22963_H
#ifndef SETTARGETFRAMERATE_TCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0_H
#define SETTARGETFRAMERATE_TCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetTargetFramerate
struct  SetTargetFramerate_tCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 SetTargetFramerate::m_TargetFrameRate
	int32_t ___m_TargetFrameRate_4;

public:
	inline static int32_t get_offset_of_m_TargetFrameRate_4() { return static_cast<int32_t>(offsetof(SetTargetFramerate_tCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0, ___m_TargetFrameRate_4)); }
	inline int32_t get_m_TargetFrameRate_4() const { return ___m_TargetFrameRate_4; }
	inline int32_t* get_address_of_m_TargetFrameRate_4() { return &___m_TargetFrameRate_4; }
	inline void set_m_TargetFrameRate_4(int32_t value)
	{
		___m_TargetFrameRate_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTARGETFRAMERATE_TCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0_H
#ifndef TESTCAMERAIMAGE_TB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6_H
#define TESTCAMERAIMAGE_TB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestCameraImage
struct  TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.RawImage TestCameraImage::m_RawImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___m_RawImage_4;
	// UnityEngine.UI.Text TestCameraImage::m_ImageInfo
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_ImageInfo_5;
	// UnityEngine.Texture2D TestCameraImage::m_Texture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___m_Texture_6;

public:
	inline static int32_t get_offset_of_m_RawImage_4() { return static_cast<int32_t>(offsetof(TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6, ___m_RawImage_4)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_m_RawImage_4() const { return ___m_RawImage_4; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_m_RawImage_4() { return &___m_RawImage_4; }
	inline void set_m_RawImage_4(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___m_RawImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_RawImage_4), value);
	}

	inline static int32_t get_offset_of_m_ImageInfo_5() { return static_cast<int32_t>(offsetof(TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6, ___m_ImageInfo_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_ImageInfo_5() const { return ___m_ImageInfo_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_ImageInfo_5() { return &___m_ImageInfo_5; }
	inline void set_m_ImageInfo_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_ImageInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ImageInfo_5), value);
	}

	inline static int32_t get_offset_of_m_Texture_6() { return static_cast<int32_t>(offsetof(TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6, ___m_Texture_6)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_m_Texture_6() const { return ___m_Texture_6; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_m_Texture_6() { return &___m_Texture_6; }
	inline void set_m_Texture_6(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___m_Texture_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Texture_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTCAMERAIMAGE_TB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6_H
#ifndef TOUCHCAMERA_T5804FE07848FB1046EA19DC751DF6A59C5F5FB14_H
#define TOUCHCAMERA_T5804FE07848FB1046EA19DC751DF6A59C5F5FB14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchCamera
struct  TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Nullable`1<UnityEngine.Vector2>[] TouchCamera::oldTouchPositions
	Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* ___oldTouchPositions_4;
	// UnityEngine.Vector2 TouchCamera::oldTouchVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oldTouchVector_5;
	// System.Single TouchCamera::oldTouchDistance
	float ___oldTouchDistance_6;

public:
	inline static int32_t get_offset_of_oldTouchPositions_4() { return static_cast<int32_t>(offsetof(TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14, ___oldTouchPositions_4)); }
	inline Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* get_oldTouchPositions_4() const { return ___oldTouchPositions_4; }
	inline Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6** get_address_of_oldTouchPositions_4() { return &___oldTouchPositions_4; }
	inline void set_oldTouchPositions_4(Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* value)
	{
		___oldTouchPositions_4 = value;
		Il2CppCodeGenWriteBarrier((&___oldTouchPositions_4), value);
	}

	inline static int32_t get_offset_of_oldTouchVector_5() { return static_cast<int32_t>(offsetof(TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14, ___oldTouchVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oldTouchVector_5() const { return ___oldTouchVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oldTouchVector_5() { return &___oldTouchVector_5; }
	inline void set_oldTouchVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oldTouchVector_5 = value;
	}

	inline static int32_t get_offset_of_oldTouchDistance_6() { return static_cast<int32_t>(offsetof(TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14, ___oldTouchDistance_6)); }
	inline float get_oldTouchDistance_6() const { return ___oldTouchDistance_6; }
	inline float* get_address_of_oldTouchDistance_6() { return &___oldTouchDistance_6; }
	inline void set_oldTouchDistance_6(float value)
	{
		___oldTouchDistance_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHCAMERA_T5804FE07848FB1046EA19DC751DF6A59C5F5FB14_H
#ifndef UIMANAGER_TA871F1BE896F9D7434A52E7413E3C9F11E6B323C_H
#define UIMANAGER_TA871F1BE896F9D7434A52E7413E3C9F11E6B323C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIManager
struct  UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.XR.ARFoundation.ARPlaneManager UIManager::m_PlaneManager
	ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * ___m_PlaneManager_6;
	// UnityEngine.Animator UIManager::m_MoveDeviceAnimation
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___m_MoveDeviceAnimation_7;
	// UnityEngine.Animator UIManager::m_TapToPlaceAnimation
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___m_TapToPlaceAnimation_8;
	// System.Boolean UIManager::m_ShowingTapToPlace
	bool ___m_ShowingTapToPlace_10;
	// System.Boolean UIManager::m_ShowingMoveDevice
	bool ___m_ShowingMoveDevice_11;

public:
	inline static int32_t get_offset_of_m_PlaneManager_6() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_PlaneManager_6)); }
	inline ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * get_m_PlaneManager_6() const { return ___m_PlaneManager_6; }
	inline ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A ** get_address_of_m_PlaneManager_6() { return &___m_PlaneManager_6; }
	inline void set_m_PlaneManager_6(ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * value)
	{
		___m_PlaneManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlaneManager_6), value);
	}

	inline static int32_t get_offset_of_m_MoveDeviceAnimation_7() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_MoveDeviceAnimation_7)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_m_MoveDeviceAnimation_7() const { return ___m_MoveDeviceAnimation_7; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_m_MoveDeviceAnimation_7() { return &___m_MoveDeviceAnimation_7; }
	inline void set_m_MoveDeviceAnimation_7(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___m_MoveDeviceAnimation_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_MoveDeviceAnimation_7), value);
	}

	inline static int32_t get_offset_of_m_TapToPlaceAnimation_8() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_TapToPlaceAnimation_8)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_m_TapToPlaceAnimation_8() const { return ___m_TapToPlaceAnimation_8; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_m_TapToPlaceAnimation_8() { return &___m_TapToPlaceAnimation_8; }
	inline void set_m_TapToPlaceAnimation_8(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___m_TapToPlaceAnimation_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_TapToPlaceAnimation_8), value);
	}

	inline static int32_t get_offset_of_m_ShowingTapToPlace_10() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_ShowingTapToPlace_10)); }
	inline bool get_m_ShowingTapToPlace_10() const { return ___m_ShowingTapToPlace_10; }
	inline bool* get_address_of_m_ShowingTapToPlace_10() { return &___m_ShowingTapToPlace_10; }
	inline void set_m_ShowingTapToPlace_10(bool value)
	{
		___m_ShowingTapToPlace_10 = value;
	}

	inline static int32_t get_offset_of_m_ShowingMoveDevice_11() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_ShowingMoveDevice_11)); }
	inline bool get_m_ShowingMoveDevice_11() const { return ___m_ShowingMoveDevice_11; }
	inline bool* get_address_of_m_ShowingMoveDevice_11() { return &___m_ShowingMoveDevice_11; }
	inline void set_m_ShowingMoveDevice_11(bool value)
	{
		___m_ShowingMoveDevice_11 = value;
	}
};

struct UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane> UIManager::s_Planes
	List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * ___s_Planes_9;

public:
	inline static int32_t get_offset_of_s_Planes_9() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields, ___s_Planes_9)); }
	inline List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * get_s_Planes_9() const { return ___s_Planes_9; }
	inline List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B ** get_address_of_s_Planes_9() { return &___s_Planes_9; }
	inline void set_s_Planes_9(List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * value)
	{
		___s_Planes_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_Planes_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIMANAGER_TA871F1BE896F9D7434A52E7413E3C9F11E6B323C_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67), -1, sizeof(ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2700[5] = 
{
	ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67::get_offset_of_m_FeatheringWidth_4(),
	ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67_StaticFields::get_offset_of_s_FeatheringUVs_5(),
	ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67_StaticFields::get_offset_of_s_Vertices_6(),
	ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67::get_offset_of_m_PlaneMeshVisualizer_7(),
	ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67::get_offset_of_m_FeatheredPlaneMaterial_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2701[7] = 
{
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_ARSession_4(),
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_ErrorText_5(),
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_LogText_6(),
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_MappingStatusText_7(),
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_SaveButton_8(),
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_LoadButton_9(),
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_LogMessages_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[4] = 
{
	U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B::get_offset_of_U3CU3E1__state_0(),
	U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B::get_offset_of_U3CU3E2__current_1(),
	U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B::get_offset_of_U3CU3E4__this_2(),
	U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B::get_offset_of_U3CrequestU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[8] = 
{
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CU3E1__state_0(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CU3E2__current_1(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CU3E4__this_2(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CsessionSubsystemU3E5__2_3(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CbytesPerFrameU3E5__3_4(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CbytesRemainingU3E5__4_5(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CbinaryReaderU3E5__5_6(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CallBytesU3E5__6_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2704[3] = 
{
	TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14::get_offset_of_oldTouchPositions_4(),
	TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14::get_offset_of_oldTouchVector_5(),
	TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14::get_offset_of_oldTouchDistance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (CameraConfigController_t7D9D87FE02FA864F391F3BB2B9E9C7923F85279B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2705[2] = 
{
	CameraConfigController_t7D9D87FE02FA864F391F3BB2B9E9C7923F85279B::get_offset_of_m_ConfigurationNames_4(),
	CameraConfigController_t7D9D87FE02FA864F391F3BB2B9E9C7923F85279B::get_offset_of_m_Dropdown_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (DisableVerticalPlanes_t40082019580280FBDED0DF62CC5DA4E1A8E32D19), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2706[1] = 
{
	DisableVerticalPlanes_t40082019580280FBDED0DF62CC5DA4E1A8E32D19::get_offset_of_m_LogText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[4] = 
{
	LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319::get_offset_of_U3CbrightnessU3Ek__BackingField_4(),
	LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319::get_offset_of_U3CcolorTemperatureU3Ek__BackingField_5(),
	LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319::get_offset_of_U3CcolorCorrectionU3Ek__BackingField_6(),
	LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319::get_offset_of_m_Light_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2708[5] = 
{
	LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4::get_offset_of_m_BrightnessText_4(),
	LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4::get_offset_of_m_ColorTemperatureText_5(),
	LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4::get_offset_of_m_ColorCorrectionText_6(),
	0,
	LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4::get_offset_of_m_LightEstimation_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3), -1, sizeof(MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2709[4] = 
{
	MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3::get_offset_of_m_Content_4(),
	MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3::get_offset_of_m_Rotation_5(),
	MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3_StaticFields::get_offset_of_s_Hits_6(),
	MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3::get_offset_of_m_SessionOrigin_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC), -1, sizeof(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2710[5] = 
{
	PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC::get_offset_of_m_PlacedPrefab_4(),
	PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC::get_offset_of_U3CspawnedObjectU3Ek__BackingField_5(),
	PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_StaticFields::get_offset_of_onPlacedObject_6(),
	PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC::get_offset_of_m_SessionOrigin_7(),
	PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_StaticFields::get_offset_of_s_Hits_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2), -1, sizeof(PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2711[4] = 
{
	PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2::get_offset_of_m_PlacedPrefab_4(),
	PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2::get_offset_of_U3CspawnedObjectU3Ek__BackingField_5(),
	PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2_StaticFields::get_offset_of_s_Hits_6(),
	PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2::get_offset_of_m_SessionOrigin_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD), -1, sizeof(PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2712[3] = 
{
	PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD::get_offset_of_m_TogglePlaneDetectionText_4(),
	PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD::get_offset_of_m_ARPlaneManager_5(),
	PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD_StaticFields::get_offset_of_s_Planes_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2713[5] = 
{
	RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB::get_offset_of_m_MakeAppearOnPlane_4(),
	RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB::get_offset_of_m_Slider_5(),
	RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB::get_offset_of_m_Text_6(),
	RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB::get_offset_of_m_Min_7(),
	RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB::get_offset_of_m_Max_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2714[5] = 
{
	ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A::get_offset_of_m_Slider_4(),
	ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A::get_offset_of_m_Text_5(),
	ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A::get_offset_of_m_Min_6(),
	ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A::get_offset_of_m_Max_7(),
	ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A::get_offset_of_m_SessionOrigin_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (ScreenCapture_t2374541D6035304D83868BE2B54D3C747DF22963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2715[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (SetTargetFramerate_tCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2716[1] = 
{
	SetTargetFramerate_tCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0::get_offset_of_m_TargetFrameRate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2717[3] = 
{
	TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6::get_offset_of_m_RawImage_4(),
	TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6::get_offset_of_m_ImageInfo_5(),
	TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6::get_offset_of_m_Texture_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2718[7] = 
{
	0,
	0,
	0,
	FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A::get_offset_of_m_Animator_7(),
	FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A::get_offset_of_m_Plane_8(),
	FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A::get_offset_of_m_ShowTime_9(),
	FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A::get_offset_of_m_UpdatingPlane_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C), -1, sizeof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2719[8] = 
{
	0,
	0,
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_PlaneManager_6(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_MoveDeviceAnimation_7(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_TapToPlaceAnimation_8(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields::get_offset_of_s_Planes_9(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_ShowingTapToPlace_10(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_ShowingMoveDevice_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2720[34] = 
{
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_Url_4(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_ShouldAutoPlay_5(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of__audioVolume_6(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_audioSourceOffset_7(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_flipHandedness_8(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of__clockScale_9(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_computeNormals_10(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_Settings_11(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_DefaultMaxVertexCount_12(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_DefaultMaxIndexCount_13(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_localSpaceBounds_14(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_meshes_15(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_meshFilter_16(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_meshRenderer_17(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_HVCollider_18(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_material_19(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_listener_20(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_pluginInterop_21(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_isInitialized_22(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_openInfo_23(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_fileInfo_24(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_lastFrameInfo_25(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_OnOpenNotify_26(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_OnUpdateFrameInfoNotify_27(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_OnRenderCallback_28(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_OnFatalError_29(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_OnEndOfStreamNotify_30(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_UnityBufferCoroutine_31(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_logger_32(),
	0,
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_wasPlaying_34(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_ShouldPauseAfterPlay_35(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_PauseFrameID_36(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_isInstanceDisposed_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (OnOpenEvent_t9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (OnFrameInfoEvent_t254A414B4783E99BE1CDD836552B5BEE9BB14C3F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (OnRenderEvent_tEBC1B611E970CED1D3F6AE62BCC833366AAC60F6), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (OnEndOfStream_t6ABE2224F14F21D78185646112D85DE309AA9091), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (OnFatalErrorEvent_tC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (U3CFillUnityBuffersU3Ed__48_t43B9E283B7914A96EDF86C9C3B4E39161E387017), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2726[3] = 
{
	U3CFillUnityBuffersU3Ed__48_t43B9E283B7914A96EDF86C9C3B4E39161E387017::get_offset_of_U3CU3E1__state_0(),
	U3CFillUnityBuffersU3Ed__48_t43B9E283B7914A96EDF86C9C3B4E39161E387017::get_offset_of_U3CU3E2__current_1(),
	U3CFillUnityBuffersU3Ed__48_t43B9E283B7914A96EDF86C9C3B4E39161E387017::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (HVConductor_t966F2F4E311B562E0F5CE66AEEC767CD63E9D59E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2727[3] = 
{
	HVConductor_t966F2F4E311B562E0F5CE66AEEC767CD63E9D59E::get_offset_of_ShouldLoopSequences_4(),
	HVConductor_t966F2F4E311B562E0F5CE66AEEC767CD63E9D59E::get_offset_of_sequenceList_5(),
	HVConductor_t966F2F4E311B562E0F5CE66AEEC767CD63E9D59E::get_offset_of_currSequence_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (HVSequence_tA5A0E5C759DEDC1FE7B4B517AB3523A256C96BA1)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2728[1] = 
{
	HVSequence_tA5A0E5C759DEDC1FE7B4B517AB3523A256C96BA1::get_offset_of_VideosToLoad_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (HCapSettingsInterop_tD574A2802911FCB0A5F59921EE9264B708ECDF4F)+ sizeof (RuntimeObject), sizeof(HCapSettingsInterop_tD574A2802911FCB0A5F59921EE9264B708ECDF4F ), 0, 0 };
extern const int32_t g_FieldOffsetTable2729[2] = 
{
	HCapSettingsInterop_tD574A2802911FCB0A5F59921EE9264B708ECDF4F::get_offset_of_defaultMaxVertexCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HCapSettingsInterop_tD574A2802911FCB0A5F59921EE9264B708ECDF4F::get_offset_of_defaultMaxIndexCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9)+ sizeof (RuntimeObject), sizeof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2730[16] = 
{
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_AudioDisabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_UseHWTexture_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_UseHWDecode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_UseKeyedMutex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_RenderViaClock_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_OutputNormals_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_StartDownloadOnOpen_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_AutoLooping_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_lockHWTextures_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_forceSoftwareClock_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_PlaybackRate_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_HRTFMinGain_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_HRTFMaxGain_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_HRTFGainDistance_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_HRTFCutoffDistance_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_AudioDeviceId_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366)+ sizeof (RuntimeObject), sizeof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2731[16] = 
{
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_hasAudio_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_duration100ns_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_frameCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_maxVertexCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_maxIndexCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_bitrateMbps_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_fileSize_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_minX_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_minY_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_minZ_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_maxX_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_maxY_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_maxZ_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_fileWidth_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_fileHeight_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_hasNormals_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (SVFReaderStateInterop_t7766360500DB4DD4843D984E227AB74F026ACEA7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2732[12] = 
{
	SVFReaderStateInterop_t7766360500DB4DD4843D984E227AB74F026ACEA7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (VRChannel_t5144073EA223E4E52EB5A04144E13FFCB55C9496)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2733[6] = 
{
	VRChannel_t5144073EA223E4E52EB5A04144E13FFCB55C9496::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696)+ sizeof (RuntimeObject), sizeof(SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2734[6] = 
{
	SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696::get_offset_of_isLiveSVFSource_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696::get_offset_of_lastReadFrame_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696::get_offset_of_unsuccessfulReadFrameCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696::get_offset_of_droppedFrameCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696::get_offset_of_errorHresult_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696::get_offset_of_lastKnownState_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E)+ sizeof (RuntimeObject), sizeof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E ), 0, 0 };
extern const int32_t g_FieldOffsetTable2735[15] = 
{
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_frameTimestamp_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_minX_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_minY_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_minZ_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_maxX_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_maxY_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_maxZ_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_frameId_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_vertexCount_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_indexCount_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_textureWidth_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_textureHeight_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_isEOS_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_isRepeatedFrame_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_isKeyFrame_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2)+ sizeof (RuntimeObject), sizeof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2736[16] = 
{
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m00_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m01_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m02_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m03_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m10_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m11_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m12_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m13_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m20_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m21_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m22_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m23_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m30_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m31_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m32_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m33_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA)+ sizeof (RuntimeObject), sizeof(Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA ), 0, 0 };
extern const int32_t g_FieldOffsetTable2737[3] = 
{
	Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA::get_offset_of_z_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (SVFPlaybackState_t0D3DA5DF6EED7D29EE64D6C40C895D3155698B6F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2738[8] = 
{
	SVFPlaybackState_t0D3DA5DF6EED7D29EE64D6C40C895D3155698B6F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (LogLevel_t614E5F716939DAB7D93BA30FEC1372E436EC62DE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2739[6] = 
{
	LogLevel_t614E5F716939DAB7D93BA30FEC1372E436EC62DE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (AudioDeviceInfo_t105929EDE9CEF6D58A0CBF914F72C1C1B608D036)+ sizeof (RuntimeObject), sizeof(AudioDeviceInfo_t105929EDE9CEF6D58A0CBF914F72C1C1B608D036_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2740[2] = 
{
	AudioDeviceInfo_t105929EDE9CEF6D58A0CBF914F72C1C1B608D036::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AudioDeviceInfo_t105929EDE9CEF6D58A0CBF914F72C1C1B608D036::get_offset_of_Id_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (SVFPlaybackStateHelper_t38FB181A4A2C8E48930305CA86FC64E2DF8ED3EE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (SVFReaderStateHelper_tF26BD4D553776D5DB620A2325BF3625E57D30684), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8), -1, sizeof(SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2743[6] = 
{
	0,
	SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8_StaticFields::get_offset_of_s_logstack_1(),
	SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8::get_offset_of_logger_2(),
	0,
	SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8::get_offset_of_instanceId_4(),
	SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8::get_offset_of_isInstanceDisposed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (Json_tB4C7A53B66AB0AFB87D47818778AF9AB10586B7B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (Parser_t3B57A69142237AEB04F76D09DD2581735AE34413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2745[2] = 
{
	0,
	Parser_t3B57A69142237AEB04F76D09DD2581735AE34413::get_offset_of_json_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (TOKEN_t53209D188AFD8042884B6D26CC7E3146600F5182)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2746[13] = 
{
	TOKEN_t53209D188AFD8042884B6D26CC7E3146600F5182::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (Serializer_tBBDA1DA8B8B2CC192031334142175F9F822049F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[1] = 
{
	Serializer_tBBDA1DA8B8B2CC192031334142175F9F822049F4::get_offset_of_builder_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
